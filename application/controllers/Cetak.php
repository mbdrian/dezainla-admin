<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model(array('auth_model','order_model','produk_model', 'konsumen_model'));
		$this->load->library(array('session','form_validation','encryption','PHPExcel/PHPExcel'));
		$this->load->helper(array('url','form','download'));
		date_default_timezone_set('Asia/Jakarta');
	}
	
	function print_rekapitulasi_bulanan(){
		$tanggal_mulai=$this->uri->segment(3);
		$tanggal_selesai=$this->uri->segment(4);
		$data['tanggal_mulai']=$this->uri->segment(3);
		$data['tanggal_selesai']=$this->uri->segment(4);
		$data['rekapitulasi_order']=$this->order_model->select_all_order_by_daterange($tanggal_mulai,$tanggal_selesai);
		$this->load->view('print/print_rekapitulasi_harian',$data);
	}

	function print_rekapitulasi_keuangan(){
		$tanggal_mulai=$this->uri->segment(3);
		$tanggal_selesai=$this->uri->segment(4);
		$data['tanggal_mulai']=$this->uri->segment(3);
		$data['tanggal_selesai']=$this->uri->segment(4);
		$data['rekapitulasi_keuangan']=$this->order_model->select_all_order_by_daterange($tanggal_mulai,$tanggal_selesai);
		$this->load->view('print/print_rekapitulasi_keuangan',$data);
	}

	function faktur_order(){
		$id_detail_order=$this->uri->segment(3);
		$data['user']=$this->order_model->select_info_user_detail($id_detail_order);
		$data['detail_order']=$this->order_model->tracking_detail_by_id($id_detail_order);
		$data['total']=$this->order_model->select_detail_order_by_id($id_detail_order);
		$konsumen=$this->order_model->select_info_user_detail($id_detail_order);
		foreach ($konsumen->result() as $value) {
			# code...
			$nama_konsumen=$value->nama_konsumen;
		}
		$total=$this->order_model->select_detail_order_by_id($id_detail_order);
		foreach ($total->result() as $value) {
			# code...
			$keterangan=$value->keterangan;
		}
		$data['keterangan']=$keterangan;
		$data['nama_konsumen']=$nama_konsumen;
		$data['admin']=$this->session->userdata('username');
		$this->load->view('print/faktur_order',$data);
	}
	

    // function pdf_per_bulan(){
	// 	//$image=base_url().'/assets/images/logo.png';
	// 	$id_user= $this->session->userdata('username');
	// 	$tanggal_mulai=$this->input->post('tanggal_mulai');
	// 	$tanggal_selesai=$this->input->post('tanggal_selesai');
	// 	$tanggal_mulai_baru=tgl_indo(date('Y-m-d',strtotime($tanggal_mulai)));
	// 	$tanggal_selesai_baru=tgl_indo(date('Y-m-d',strtotime($tanggal_selesai)));
	// 	$rekapitulasi_order=$this->order_model->select_all_order_by_daterange($tanggal_mulai,$tanggal_selesai);
	// 	$image=base_url().'/assets/images/logo.jpg';
	// 	$this->fpdf->FPDF('L','cm','A5');
	// 	$this->fpdf->AddPage();
	// 	$this->fpdf->SetMargins(2,2,2);
	// 	$this->fpdf->Ln();
	// 	$this->fpdf->setFont('Arial','B',9);
	// 	$this->fpdf->Text(7,1,'LAPORAN PENJUALAN DEZAINLA.COM','C');//left//top
	// 	$this->fpdf->setFont('Arial','B',9);
	// 	$this->fpdf->Text(4.5,1.5,'Rekapitulasi Order Dari Tanggal '.$tanggal_mulai_baru.' Sampai Tanggal '.$tanggal_selesai_baru.'','C');//left//top
	// 	$this->fpdf->setFont('Arial','',7);
	// 	$this->fpdf->Text(6,2,'Alamat: Jl. R.A Abusamah No. 87 Kel. Sukabangun II, Kec. Sukarame, Palembang','C');//left//top
	// 	$this->fpdf->setFont('Arial','',7);
	// 	$this->fpdf->Text(8.1,1.9,'');
	// 	$this->fpdf->Line(20.3,2.1,0.5,2.1); 
	// 	$this->fpdf->Cell(0,0,$this->fpdf->Image($image,1, 0.1, 2), 0, 0, 'L', false );
	// 	$this->fpdf->ln(2.0);
	// 	$this->fpdf->setFont('Arial','B',7);
	// 	$this->fpdf->Cell(1,0.5,'No',1,0,'C');
	// 	$this->fpdf->Cell(2,0.5,'Tanggal Order',1,0,'C');
	// 	$this->fpdf->Cell(2,0.5,'ID Order',1,0,'C');
	// 	$this->fpdf->Cell(3,0.5,'Nama Konsumen',1,0,'C');
	// 	$this->fpdf->Cell(2,0.5,'No Handphone',1,0,'C');
	// 	$this->fpdf->Cell(2,0.5,'Total Bayar',1,0,'C');
	// 	$this->fpdf->Cell(2,0.5,'Uang Muka',1,0,'C');
	// 	$this->fpdf->Cell(3,0.5,'Sisa Pembayaran',1,0,'C');
	// 	$this->fpdf->Cell(3,0.5,'Penerima File',1,0,'C');
	// 	$this->fpdf->Ln();
	// 	$this->fpdf->setFont('Arial','',7);
	// 	$no=1;
	// 	$total_harga=0;
    //     $sisa_pembayaran=0;
    //     $uang_muka=0;
	// 	foreach ($rekapitulasi_order->result() as $items) {
	// 		# code...
	// 		$this->fpdf->Cell(1,0.5,''.$no++.'',1,0,'C');
	// 		$this->fpdf->Cell(2,0.5,''.$items->tanggal_order.'',1,0,'C');
	// 		$this->fpdf->Cell(2,0.5,''.$items->id_detail_order.'',1,0,'C');
	// 		$this->fpdf->Cell(3,0.5,''.$items->nama_konsumen.'',1,0,'C');
	// 		$this->fpdf->Cell(2,0.5,''.$items->hp_konsumen.'',1,0,'C');
	// 		$this->fpdf->Cell(2,0.5,''.$items->total_bayar.'',1,0,'C');
	// 		$this->fpdf->Cell(2,0.5,''.$items->uang_muka.'',1,0,'C');
	// 		$this->fpdf->Cell(3,0.5,''.$items->sisa_pembayaran.'',1,0,'C');
	// 		$this->fpdf->Cell(3,0.5,''.$items->nama.'',1,0,'C');
	// 		@$total_harga += $items->total_bayar;
    //    		@$sisa_pembayaran += $items->sisa_pembayaran;
    //     	@$uang_muka += $items->uang_muka;
	// 		$this->fpdf->Ln();
	// 	}	
	// 	$this->fpdf->Text(15,10,'Total Jumlah','C');//left//top
	// 	$this->fpdf->Text(15,10.5,'Uang Muka','C');//left//top
	// 	$this->fpdf->Text(15,11,'Sisa Pembayaran','C');//left//top
	// 	$this->fpdf->Text(17,10,' : '.$total_harga.'','C');//left//top
	// 	$this->fpdf->Text(17,10.5,' : '.$uang_muka.'','C');//left//top
	// 	$this->fpdf->Text(17,11,' : '.$sisa_pembayaran.'','C');//left//top
	// 	$this->fpdf->Ln(1.0);
	// 	$this->fpdf->setFont('Arial','',7);
	// 	$this->fpdf->Text(15,12,'Yang bertanda tangan di bawah ini,','C');//left//top
	// 	$this->fpdf->Text(15.5,12.5,'Palembang, '.tgl_indo(date('Y-m-d')).'','C');//left//top
	// 	$this->fpdf->Ln(1.0);
	// 	$this->fpdf->Text(16,14,''.$id_user.'','C');//left//top
	// 	$this->fpdf->Output("".str_replace("-","",$tanggal_mulai."/".$tanggal_selesai).".pdf","I");
    // }

    function excel_per_bulan(){
    	$tanggal_mulai=$this->input->post('tanggal_mulai');
		$tanggal_selesai=$this->input->post('tanggal_selesai');
		$query = $this->order_model->select_all_order_by_daterange($tanggal_mulai,$tanggal_selesai);
		$data = array(
			'title' => 'Rekapitulasi Order'.$tanggal_mulai.' - '.$tanggal_selesai,
			'data' => $query
		);
		$this->load->view('print/layout_excel_order.php', $data);
	}
	
	function excel_rekapitulasi_keuangan(){
    	$tanggal_mulai=$this->input->post('tanggal_mulai');
		$tanggal_selesai=$this->input->post('tanggal_selesai');
		$query = $this->order_model->select_all_keuangan_by_daterange($tanggal_mulai,$tanggal_selesai);
		$data = array(
			'title' => 'Rekapitulasi Keuangan'.$tanggal_mulai.' - '.$tanggal_selesai,
			'data' => $query
		);
		$this->load->view('print/layout_excel_keuangan.php', $data);
    }


	function excel_konsumen(){
		$query = $this->konsumen_model->select();
		$date = date("Y-m-d H:i:s");
		$data = array(
			'title' => "Data Konsumen updated last ". $date,
			'data' => $query
		);
		// print_r($query->result_array());

		$this->load->view('print/layout_excel_konsumen', $data);
	}
}
?>
