<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model(array('auth_model','order_model','produk_model'));
		date_default_timezone_set('Asia/Jakarta');
	}

	function tes(){
		$this->load->view('page/tes');
	}

	function search(){
		$keyword = urldecode($this->uri->segment(3));

		// cari di database
		$data = $this->db->from('konsumen')->like('nama_konsumen',$keyword)->get();	

		$arr = array();
		// format keluaran di dalam array
		foreach($data->result() as $row)
		{
			$arr['query'] = $keyword;
			$arr['suggestions'][] = array(
				'value'	=>$row->nama_konsumen,
				'hp_konsumen'	=>$row->hp_konsumen
			);
		}
		// minimal PHP 5.2
		echo json_encode($arr);
	}
	
	function searchByNameConsumer(){
		$keyword = urldecode($this->uri->segment(3));

		// cari di database
		$data = $this->db->from('konsumen')->where('nama_konsumen',$keyword)->get();	

		echo count($data->result());
	}

	public function index()
	{	
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login!=1){
			// echo '<script>alert("Username atau Password salah :'.print_r($this->session->all_userdata()).':");</script>';
			redirect(base_url('auth'));
		}
		$data['title']='Dashboard';
		$data['content']='dashboard';
		$data['year_now'] = date('Y');
		$data['count_total_order']=$this->order_model->select_all_order()->num_rows();
		$data['count_produk']=$this->produk_model->select_all_produk()->num_rows();
		$data['count_total_konsumen']=$this->order_model->select_count_all_konsumen()->num_rows();
		$data['chart_order']=$this->order_model->select_count_order_by_year(date('Y'));
		// $data['chart_order']=$this->order_model->select_count_order();
		$data['chart_konsumen']=$this->order_model->select_count_konsumen_by_year(date('Y'));
		// $data['chart_konsumen']=$this->order_model->select_count_konsumen();
		$this->load->view('template',$data);
	}

	function proses_cari_grafik(){
		$YearNow = $this->input->post('yearDashboard');

		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login!=1) redirect(base_url('auth'));

		$data['title']='Dashboard';
		$data['content']='dashboard';
		$data['year_now'] = $YearNow;
		$data['count_total_order']=$this->order_model->select_all_order()->num_rows();
		$data['count_produk']=$this->produk_model->select_all_produk()->num_rows();
		$data['count_total_konsumen']=$this->order_model->select_count_all_konsumen()->num_rows();
		$data['chart_order']=$this->order_model->select_count_order_by_year($YearNow);
		// $data['chart_order']=$this->order_model->select_count_order();
		$data['chart_konsumen']=$this->order_model->select_count_konsumen_by_year($YearNow);
		// $data['chart_konsumen']=$this->order_model->select_count_konsumen();
		$this->load->view('template',$data);
	}

}
?>
