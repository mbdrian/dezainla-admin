<?php
// Created by Rian

defined('BASEPATH') OR exit('No direct script access allowed');

class Konsumen extends CI_Controller {
	function __construct(){
		parent::__construct();
        $this->load->model(array('konsumen_model'));
		$this->load->library(array('session','form_validation','encryption'));
		$this->load->helper(array('url','form','download'));
		date_default_timezone_set('Asia/Jakarta');
	
	}
	
	function index() {
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login==false)redirect(base_url('auth'));
		$data['title']='Konsumen';
		$data['content']='daftar_konsumen';
        $data['konsumen'] = $this->konsumen_model->select();
		// print_r($data);
		$this->load->view('template',$data);
	}

    function edit_konsumen(){
        $nama_konsumen = $this->input->post('nama_konsumen');
        $data['hp_konsumen'] = $this->input->post('hp_konsumen');
		$nama_konsumen = urldecode($nama_konsumen);
        $this->konsumen_model->update($nama_konsumen, $data);
		$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Data '.$nama_konsumen.' berhasil diubah</div>');

        redirect(base_url('konsumen'),'refresh');
        
    }

    function hapus_konsumen(){
        $nama_konsumen=$this->uri->segment(3);
		if($nama_konsumen=='')redirect(base_url('konsumen'));
		$nama_konsumen = urldecode($nama_konsumen);
		$this->konsumen_model->hapus_konsumen($nama_konsumen);
		$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Data konsumen '.$nama_konsumen.' berhasil dihapus</div>');

		redirect(base_url('konsumen'),'refresh');
    }

	
}
