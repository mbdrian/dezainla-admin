<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model(array('auth_model','order_model','produk_model','konsumen_model','user_model'));
		$this->load->library(array('session','form_validation','encryption','datatables'));
		$this->load->helper(array('url','form','download'));
		date_default_timezone_set('Asia/Jakarta');
	}
	
	public function index()
	{	

		$data['username'] = $this->session->userdata('username');
		$data['level']    = $this->session->userdata('level');
		$data['id_user']  = $this->session->userdata('id_user');
		$login            = $this->session->userdata('login');
		$level            = $this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']         = 'Order Produk';
		$data['content']       = 'order_produk';
		$data['penerima_file'] = $this->user_model->select_all_user_by_level();
		$data['barang']        = $this->produk_model->select_all_detail_produk();
		$data['count_order']   = $this->order_model->select_all_order()->num_rows();
		$data['count_produk']  = $this->produk_model->select_all_produk()->num_rows();
		$this->load->view('template',$data);
	}

	function tes_order(){
		$this->cart->destroy();
		
		$order=$this->order_model->select_all_order();
		foreach ($order->result() as $value) {
			# code...
			$data=array(
			   'id'      => $value->id_order_produk,
               'qty'     => $value->kuantitas,
               'price'   => $value->subtotal,
               'name'    => $value->id_detail_order,
				);
			$this->cart->insert($data);
		}
		foreach ($this->cart->contents() as $items) {
				# code...
				echo $items['id']."</br>";
			}
		
		
	}

	function proses_order(){
		// print_r($this->input->post());
		// return;
		$tanggal=$this->input->post('tanggal');
		$tanggal_pengambilan=date('Y-m-d',strtotime($this->input->post('tanggal_pengambilan')));
		$waktu_pengambilan=$this->input->post('waktu_pengambilan');
		$tanggal_baru=tgl_indo(date('Y-m-d',strtotime($tanggal)));
		$nama_konsumen=$this->input->post('nama_konsumen');
		$hp_konsumen=$this->input->post('hp_konsumen');
		$total=$this->input->post('total');
		$bayar=$this->input->post('bayar');
		$kembali=$this->input->post('kembali');
		$nama_penerima_file=$this->input->post('nama_penerima_file');
		$string=strtoupper(substr(md5(rand()), 0, 7)) ;

		$this->form_validation->set_rules('nama_konsumen','nama_konsumen','required');
		$this->form_validation->set_rules('hp_konsumen','hp_konsumen','required');
		$this->form_validation->set_rules('tanggal_pengambilan','tanggal_pengambilan','required');
		$this->form_validation->set_rules('waktu_pengambilan','waktu_pengambilan','required');
		$this->form_validation->set_rules('nama_penerima_file','nama_penerima_file','required');
		if($this->form_validation->run() == true){
			foreach ($this->cart->contents() as $items){
			// $data['id_order_produk']='';
			$data['id_detail_order']=$string;
			$data['tanggal_order']=date('Y-m-d',strtotime($tanggal));
			$data['nama_konsumen']=$this->input->post('nama_konsumen');
			$data['hp_konsumen']=$this->input->post('hp_konsumen');
			$data['id_user']=$this->input->post('nama_penerima_file');
			$data['tanggal_pengambilan']=date('Y-m-d',strtotime($tanggal_pengambilan));
			$data['waktu_pengambilan']=$this->input->post('waktu_pengambilan');
			$data['id_detail_produk'] = $items['options']['id_produk'];
			$data['panjang'] =$items['options']['panjang'];
			$data['lebar'] =$items['options']['lebar'];
			$data['kuantitas'] = $items['options']['kuantitas'];
			$data['harga_akhir'] = $items['price'];
			$items['subtotal']=$items['panjang']*$items['lebar']*$items['qty']*$items['price'];
			$data['subtotal'] = $items['subtotal'];

			$insert_data=$this->order_model->insert_order($data);		
	        }
	        if ($this->input->post('kembali')=='' || $this->input->post('kembali')==0) {
	        	# code...
	        	$detail['id_detail_order']=$string;
	        	$detail['total_bayar']=str_replace(".","",$this->input->post('total'));
	        	$detail['uang_muka']=str_replace(".","",$this->input->post('bayar'));
	        	$detail['sisa_pembayaran']=str_replace(".","",$this->input->post('kembali'));
	        	$detail['status_pembayaran']='lunas';
	        	$detail['keterangan']=$this->input->post('keterangan');
	        	$insert_detail_order=$this->order_model->insert_detail_order($detail);
	        	
	        	$cek_konsumen=$this->konsumen_model->select_all_konsumen($this->input->post('nama_konsumen'));
	        	
	        	if ($cek_konsumen->num_rows()==0) {
	        		# code...
	        		$konsumen['nama_konsumen']=$this->input->post('nama_konsumen');
	        		$konsumen['hp_konsumen']=$this->input->post('hp_konsumen');
	        		$insert_konsumen=$this->konsumen_model->insert($konsumen);
	        	}
	        }
	        else{
	        	$detail['id_detail_order']=$string;
	        	$detail['total_bayar']=str_replace(".","",$this->input->post('total'));
	        	$detail['diskon']=str_replace(".","",$this->input->post('diskon'));
	        	$detail['uang_muka']=str_replace(".","",$this->input->post('bayar'));
	        	$detail['sisa_pembayaran']=str_replace(".","",$this->input->post('kembali'));
	        	$detail['status_pembayaran']='belum_lunas';
	        	$detail['status_pengambilan']='belum_diambil';
	        	$detail['keterangan']=$this->input->post('keterangan');
	        	$insert_detail_order=$this->order_model->insert_detail_order($detail);

	        	$cek_konsumen=$this->konsumen_model->select_all_konsumen($this->input->post('nama_konsumen'));
	        	
	        	if ($cek_konsumen->num_rows()==0) {
	        		# code...
	        		$konsumen['nama_konsumen']=$this->input->post('nama_konsumen');
	        		$konsumen['hp_konsumen']=$this->input->post('hp_konsumen');
	        		$insert_konsumen=$this->konsumen_model->insert($konsumen);
	        	}
	        }

			$this->cart->destroy();
			$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Transaksi berhasil diinput</div>');
			redirect(base_url('order/tracking_order'),'refresh');
		}
		else{
			$this->cart->destroy();
			$this->session->set_flashdata('status', '<div class="alert alert-danger" role="alert">Transaksi gagal diinput</div>');
			redirect(base_url('order'),'refresh');
		}
		

	}

	function hapus_data_order(){
		$id_detail_order=$this->uri->segment(4);
		$id_order_produk=$this->uri->segment(3);
		$this->order_model->delete_order($id_detail_order);
		$this->order_model->delete_detail_order($id_detail_order);
		redirect(base_url('order/tracking_order'));
	}

	function json(){
		header('Content-Type: application/json');
        echo $this->order_model->json();
	}

	function jsonWithParams(){
		header('Content-Type: application/json');
		$year = $this->input->post('year');
		$month = $this->input->post('month');
    	echo $this->order_model->jsonwithParams($year, $month);
	}

	function tracking_order(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Tracking Order';
		$data['content']='tracking_order';
		$data['tracking_order']=$this->order_model->select_all_order_by_status();
		$this->load->view('template',$data);
	}

	function set_status_pengambilan(){
		$id_detail_order=@$_POST["id_detail_order"];
		$data['status_pengambilan']='sudah_diambil';
		$this->order_model->set_status_pembayaran($id_detail_order,$data);
	}

	function rekapitulasi_order(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Rekapitulasi Order';
		$data['content']='rekapitulasi_order_bulanan';
		$this->load->view('template',$data);
	}

	function rekapitulasi_keuangan(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login = $this->session->userdata('login');
		$level = $this->session->userdata('level');
		if($level == 'produksi') redirect(base_url('dashboard'));
		if($login == false) redirect(base_url('auth'));
		$data['title'] = 'Rekapitulasi Keuangan';
		$data['content'] = 'rekapitulasi_keuangan';
		$this->load->view('template', $data);
	}

	function proses_rekapitulasi_keuangan(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Rekapitulasi Keuangan';
		$tanggal_mulai=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
		$tanggal_selesai=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
		$data['tanggal_mulai']=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
		$data['tanggal_selesai']=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
		$data['content']='rekapitulasi_keuangan';
		$data['rekapitulasi_keuangan']=$this->order_model->select_all_keuangan_by_daterange($tanggal_mulai,$tanggal_selesai);
		$this->load->view('template',$data);
	}

	function rekapitulasi_order_by_user(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Rekapitulasi Order Oleh '.$this->session->userdata('username');
		$data['content']='rekapitulasi_order_by_user';
		$this->load->view('template',$data);
	}


	function proses_rekapitulasi_order_by_user(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Rekapitulasi Order Oleh '.$this->session->userdata('username');
		$username=$this->session->userdata('id_user');
		$tanggal_mulai=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
		$tanggal_selesai=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
		$data['tanggal_mulai']=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
		$data['tanggal_selesai']=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
		$data['content']='rekapitulasi_order_by_user';
		$data['rekapitulasi_keuangan']=$this->order_model->select_all_order_by_user($tanggal_mulai,$tanggal_selesai,$username);
		$this->load->view('template',$data);
	}



	function edit_data_order(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Edit Order';
		$data['content']='edit_data_order';
		$data['penerima_file']=$this->user_model->select_all_user_by_level();
		$data['barang']=$this->produk_model->select_all_detail_produk();
		$data['count_order']=$this->order_model->select_all_order()->num_rows();
		$data['count_produk']=$this->produk_model->select_all_produk()->num_rows();
		$data['order_user']=$this->order_model->select_order_by_id($this->uri->segment(3));
		$data['detail_order']=$this->order_model->edit_detail_order($this->uri->segment(3));
		$data['detail_order_produk']=$this->order_model->edit_detail_order_produk($this->uri->segment(3));
		$data['penerima_file']=$this->user_model->select_all_user_by_level();
		$this->load->view('template',$data);
	}

	function delete_order_produk(){
		$id_detail_order=$this->uri->segment(3);
		$id_order_produk=$this->uri->segment(4);
		$subtotal=$this->uri->segment(5);
		$hasil=$this->order_model->select_detail_pembayaran($id_detail_order);
		foreach ($hasil->result() as $value) {
			# code...
			$sisa_pembayaran=$value->sisa_pembayaran;
			$total_bayar=$value->total_bayar;
			$data['sisa_pembayaran']=$sisa_pembayaran+$subtotal;
			$data['total_bayar']=$total_bayar-$subtotal;
		}
		$this->order_model->update_detail_order($id_detail_order,$data);
		$cek_order=$this->order_model->edit_detail_order($this->uri->segment(3));
		$delete_order=$this->order_model->delete_order_produk($id_order_produk);
		if ($delete_order) {
			# code...
				redirect(base_url('order/edit_data_order/".$id_order_produk."'));
		}
		else{
			?>
			<script type="text/javascript">
				window.location=history.go(-1);
			</script>
			<?php
		}

	}

	function proses_edit_data_order(){
		$id_detail_order=$this->input->post('id_detail_order');
		$data['tanggal_order']=date('Y-m-d',strtotime($this->input->post('tanggal_order')));
		$data['nama_konsumen']=$this->input->post('nama_konsumen');
		$data['hp_konsumen']=$this->input->post('hp_konsumen');
		$data['id_user']=$this->input->post('penerima_file');
		$data['tanggal_pengambilan']=date('Y-m-d',strtotime($this->input->post('tanggal_pengambilan')));
		$data['waktu_pengambilan']=$this->input->post('waktu_pengambilan');
		$detail_order['uang_muka']=$this->input->post('uang_muka');
		$detail_order['sisa_pembayaran']=$this->input->post('sisa_pembayaran');
		$detail_order['diskon']=$this->input->post('diskon');
		$detail_order['keterangan'] = $this->input->post('keterangan');

		$this->order_model->update_detail_order($id_detail_order,$detail_order);
		$this->order_model->update_data_order($id_detail_order,$data);
		
		$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Order berhasil diubah</div>');
		redirect(base_url('order/edit_data_order/'.$id_detail_order),'refresh');
		
		
		
	}

	function rekapitulasi_order_bulanan(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Rekapitulasi Order Bulanan';
		$data['content']='rekapitulasi_order_bulanan';
		$this->load->view('template',$data);
	}

	function proses_rekapitulasi_order_bulanan(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Rekapitulasi Order Bulanan';
		$tanggal_mulai=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
		$tanggal_selesai=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
		$data['tanggal_mulai']=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
		$data['tanggal_selesai']=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
		$data['content']='rekapitulasi_order_bulanan';
		$data['rekapitulasi_order']=$this->order_model->select_all_order_by_daterange($tanggal_mulai,$tanggal_selesai);
		$this->load->view('template',$data);
		
	}

	function set_tracking_order(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($this->uri->segment(3)=='')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$id_order_produk=$this->uri->segment(3);

		$data['title']='Tracking Order';
		$data['content']='set_tracking_order';
		$data['tracking_order']=$this->order_model->set_tracking_order_produk($id_order_produk);
		$this->load->view('template',$data);
	}

	function proses_set_tracking_order(){
		$id_order_produk=$this->input->post('id_order_produk');
		if($id_order_produk=='')redirect(base_url('dashboard'));
		$data['status']=$this->input->post('status');
		$this->order_model->proses_set_tracking_order($id_order_produk,$data);
		?>
		<script>
			alert("Data Berhasil Diubah");
		</script>
		<?php
		if ($this->session->userdata('level')=='desainer') {
			# code...
			redirect(base_url('order/rekapitulasi_order_by_user'),'refresh');
		}
		else{
			redirect(base_url('order/tracking_order_status'),'refresh');
		}
		
	}

	function json_tracking_order(){
		header('Content-Type: application/json');
        echo $this->order_model->json_tracking_order();
	}

	function tracking_order_status(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($login==false)redirect(base_url('auth'));

		$data['title']='Tracking Order Status';
		$data['content']='set_tracking_order_status';
		$data['detail_produk']=$this->order_model->tracking_order_status();
		$this->load->view('template',$data);
	}

	function ubah_status_pengerjaan(){
		$id_order=$this->input->post('id');
		if($id_order=='')redirect(base_url('dashboard'));
		$data['detail_order']=$this->order_model->status_pengerjaan($id_order);
		$this->load->view('page/status_pengerjaan',$data);
	}

	function tes(){
		foreach ($this->cart->contents() as $items) {
			# code...
			echo $items['options']['id_produk'];
		}
	}

	function set_status_pembayaran(){
		$id_detail_order=$this->uri->segment(3);
		$total_bayar = $this->uri->segment(4);

		if($id_detail_order=='')redirect(base_url('dashboard'));
		$data['status_pembayaran']='lunas';
		$data['sisa_pembayaran']=0;
		$data['uang_muka'] = $total_bayar;

		$this->order_model->set_status_pembayaran($id_detail_order,$data);
		redirect(base_url('order/tracking_order'),'refresh');

	}

	function tracking_detail_by_id(){
		$id_order=$this->input->post('id');
		if($id_order=='')redirect(base_url('dashboard'));
		$data['detail_order']=$this->order_model->tracking_detail_by_id($id_order);
		$data['detail_pembayaran']=$this->order_model->select_detail_pembayaran($id_order);
		$this->load->view('page/detail_tracking',$data);
	}

	function json_tracking_deadline(){
		header('Content-Type: application/json');
        echo $this->order_model->json_tracking_deadline();
	}

	function tracking_deadline(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login==false)redirect(base_url('auth'));

		$data['title']='Tracking Deadline Order Status';
		$data['tracking_order']=$this->order_model->tracking_deadline();
		$data['content']='tracking_deadline';
		$this->load->view('template',$data);
	}

	function json_tracking_deadline_lewat(){
		header('Content-Type: application/json');
        echo $this->order_model->json_tracking_deadline_lewat();
	}

	function tracking_deadline_lewat(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login==false)redirect(base_url('auth'));

		$data['title']='Tracking Deadline Order Status';
		$data['tracking_order']=$this->order_model->tracking_deadline_lewat();
		$data['content']='tracking_deadline_lewat';
		$this->load->view('template',$data);
	}

	function edit_tracking_deadline_lewat(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login==false)redirect(base_url('auth'));
		$id_detail_order=$this->uri->segment(3);

		$data['title']='Tracking Deadline Order Status';
		$data['tracking_order']=$this->order_model->edit_tracking_deadline_lewat($id_detail_order);
		$data['content']='edit_tracking_deadline_lewat';
		$this->load->view('template',$data);
	}

	function proses_tanggal_deadline_baru(){
		$id_detail_order=$this->input->post('id_detail_order');
		$data['tanggal_pengambilan']=date('Y-m-d',strtotime($this->input->post('tanggal_deadline_baru')));
		$this->form_validation->set_rules('tanggal_deadline_baru','tanggal_deadline_baru','required');
		if($this->form_validation->run() == false){
			redirect(base_url('order/tracking_deadline_lewat'));
		}
		else{
			$this->order_model->proses_tanggal_deadline_baru($id_detail_order,$data);
			$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Tanggal berhasil diubah</div>');
			redirect(base_url('order/tracking_deadline_lewat'));	
		}
	}

	function cari_produk(){
		$kode=$this->input->post('kode',TRUE);

 		$query=$this->produk_model->select_produk_by_nama($kode);
  		$json_array = array();
        foreach ($query->result_array() as $row)
            $json_array[]=$row['jenis_produk'];
        echo json_encode($json_array);
	}

	public function data(){
 		 $kode=$this->input->post('kode',TRUE);

  		$query=$this->produk_model->cari_judul($kode);
  		$json_array = array();
        foreach ($query as $row)
            $json_array[]=$row->jenis_produk;
        echo json_encode($json_array);
 	}

 	function getbarang($id){
 		$barang = $this->produk_model->select_produk_by_id($id);

		if ($barang) {
			foreach ($barang->result_array() as $value) {
				# code...
			
			echo '<div class="form-group">
				      <label class="control-label col-md-3" 
				      	for="nama_barang">Nama Barang :</label>
				      <div class="col-md-8">
				        <input type="text" class="form-control reset" 
				        	name="nama_barang" id="nama_barang" 
				        	value="'.$value['jenis_produk'].'"
				        	readonly="readonly">
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="control-label col-md-3" 
				      	for="harga_barang">Harga (Rp) :</label>
				      <div class="col-md-8">
				        <input type="text" class="form-control reset" id="harga_barang" name="harga_barang" 
				        	value="'.number_format( $value['harga_produk'], 0 ,
				        	 '' , '.' ).'">
				      </div>
				    </div>
				    <div class="form-group">
                      <label class="control-label col-md-3" 
                        for="qty">Panjang :</label>
                      <div class="col-md-4">
                        <input type="number" class="form-control reset" 
                            autocomplete="off" onchange="subTotal(this.value)" 
                            onkeyup="subTotal(this.value)" id="panjang" min="0" 
                            name="panjang" placeholder="Isi Panjang..." value="1">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                        for="qty">Lebar :</label>
                      <div class="col-md-4">
                        <input type="number" class="form-control reset" 
                            autocomplete="off" onchange="subTotal(this.value)" 
                            onkeyup="subTotal(this.value)" id="lebar" min="0" 
                            name="lebar" placeholder="Isi Lebar..." value="1">
                      </div>
                    </div>
				    <div class="form-group">
				      <label class="control-label col-md-3" 
				      	for="qty">Quantity :</label>
				      <div class="col-md-4">
				        <input type="number" class="form-control reset" 
				        	name="qty" placeholder="Isi qty..." autocomplete="off" 
				        	id="qty" onchange="subTotal(this.value)" 
				        	onkeyup="subTotal(this.value)">
				      </div>
				    </div>
				    ';
			}
	    }else{

	    	echo '<div class="form-group">
				      <label class="control-label col-md-3" 
				      	for="nama_barang">Nama Barang :</label>
				      <div class="col-md-8">
				        <input type="text" class="form-control reset" 
				        	name="nama_barang" id="nama_barang" 
				        	readonly="readonly">
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="control-label col-md-3" 
				      	for="harga_barang">Harga (Rp) :</label>
				      <div class="col-md-8">
				        <input type="text" class="form-control reset" 
				        	name="harga_barang" id="harga_barang" 
				        	readonly="readonly">
				      </div>
				    </div>
				    <div class="form-group">
				      <label class="control-label col-md-3" 
				      	for="qty">Quantity :</label>
				      <div class="col-md-4">
				        <input type="number" class="form-control reset" 
				        	autocomplete="off" onchange="subTotal(this.value)" 
				        	onkeyup="subTotal(this.value)" id="qty" min="0"  
				        	name="qty" placeholder="Isi qty...">
				      </div>
				    </div>';
	    }
 	}

 	public function addbarang()
	{

		$data = array(
				'id' => str_shuffle($this->input->post('id_barang')),
				'name' => $this->input->post('nama_barang'),
				'panjang' => $this->input->post('panjang'),
				'lebar' => $this->input->post('lebar'),
				'price' => str_replace('.', '', $this->input->post('harga_barang')),
				'qty' => $this->input->post('qty'),
				'options'=>array(
					'id_produk'=>$this->input->post('id_barang'),
					'nama_barang'=>$this->input->post('nama_barang'),
					'panjang'=>$this->input->post('panjang'),
					'lebar'=>$this->input->post('lebar'),
					'harga_produk'=>$this->input->post('harga_barang'),
					'kuantitas'=>$this->input->post('qty'),
					)
				);
		$insert = $this->cart->insert($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_list_transaksi()
	{

		$data = array();

		$no = 1; 
        
        foreach ($this->cart->contents() as $items){
        	
			$row = array();
			$row[] = $no;
			$row[] = $items["id"];
			$row[] = $items["name"];
			$row[] = 'Rp. ' . number_format( $items['price'], 
                    0 , '' , '.' ) . ',-';
			$row[] = $items["qty"];
			$items['subtotal']=$items['panjang']*$items['lebar']*$items['qty']*$items['price'];
			$row[] = 'Rp. ' . number_format($items['subtotal'], 
					0 , '' , '.' ) . ',-';

			//add html for action
			$row[] = '<a 
				href="javascript:void()" style="color:rgb(255,128,128);
				text-decoration:none" onclick="deletebarang('
					."'".$items["rowid"]."'".','."'".$items['subtotal'].
					"'".')"> <i class="fa fa-close"></i> Delete</a>';
		
			$data[] = $row;
			$no++;
        }

		$output = array(
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit_transaksi()
	{

		$id_detail_order=$this->uri->segment(4);
		$order=$this->order_model->select_order_by_id_etail($id_detail_order);

		foreach ($order->result() as $value) {
			# code...
			
			
			$coba=array(
			   'id'      => $value->id_detail_produk,
			   'panjang'      => $value->panjang,
			   'lebar'      => $value->lebar,
               'qty'     => $value->kuantitas,
               'price'   => $value->subtotal,
               'name'   => $value->jenis_produk
				);
			$this->cart->insert($coba);
			
		}
		
		$data = array();

		$no = 1; 
        
        foreach ($this->cart->contents() as $items){
			$row = array();
			$row[] = $no;
			$row[] = $items["id"];
			$row[] = $items["name"];
			$row[] = 'Rp. ' . number_format( $items['price'], 
                    0 , '' , '.' ) . ',-';
			$row[] = $items["qty"];
			$items['subtotal']=$items['panjang']*$items['lebar']*$items['qty']*$items['price'];
			$row[] = 'Rp. ' . number_format($items['subtotal'], 
					0 , '' , '.' ) . ',-';

			//add html for action
			$row[] = '<a 
				href="javascript:void()" style="color:rgb(255,128,128);
				text-decoration:none" onclick="deletebarang('
					."'".$items["rowid"]."'".','."'".$items['subtotal'].
					"'".')"> <i class="fa fa-close"></i> Delete</a>';
		
			$data[] = $row;
			$no++;
			
        }

		$output = array(
					"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function deletebarang($rowid) 
	{

		$this->cart->update(array(
				'rowid'=>$rowid,
				'qty'=>0,));
		echo json_encode(array("status" => TRUE));
	}
}
?>