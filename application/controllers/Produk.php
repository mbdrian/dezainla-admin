<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model(array('auth_model','produk_model'));
		$this->load->library(array('session','form_validation','encryption','datatables'));
		$this->load->helper(array('url','form','download'));		
		date_default_timezone_set('Asia/Jakarta');
	}

	public function daftar_produk()
	{	
		
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($login==false)redirect(base_url('auth'));
		
		$data['title']='Daftar Produk';
		$data['content']='daftar_produk';
		$data['detail_produk']=$this->produk_model->select_all_detail_produk();
		$this->load->view('template',$data);
	}

	function json() {
        header('Content-Type: application/json');
        echo $this->produk_model->json();
    }

	function tambah_produk(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($login==false)redirect(base_url('auth'));
		
		$data['title']='Tambah Produk';
		$data['content']='tambah_produk';
		$data['produk']=$this->produk_model->select_all_produk();
		$this->load->view('template',$data);
	}

	function proses_tambah_produk(){
		$id_detail_produk=$this->input->post('id_detail_produk');
		$data['id_detail_produk']=$this->input->post('id_detail_produk');
		$data['jenis_produk']=$this->input->post('jenis_produk');
		$data['harga_produk']=$this->input->post('harga_produk');
		$data['keterangan']=$this->input->post('keterangan');
		$data['id_produk']=$this->input->post('id_produk');
		if($this->input->post('hpp')) {
			$data['hpp']=$this->input->post('hpp');
			$this->form_validation->set_rules('hpp','hpp','required');
		}
		$this->form_validation->set_rules('id_detail_produk','id_detail_produk','required');
		$this->form_validation->set_rules('jenis_produk','jenis_produk','required');
		$this->form_validation->set_rules('harga_produk','harga_produk','required');
		$this->form_validation->set_rules('id_produk','id_produk','required');
		
		$cek_master_produk=$this->produk_model->select_produk_by_id($id_detail_produk);

		if($this->form_validation->run() == FALSE){
			?>
				<script>
					alert("Form tidak boleh ada yg kosong");
					window.location=history.go(-1);
				</script>
			<?php
		}
		else{
			if ($cek_master_produk->num_rows()==1) {
			# code...
			?>
			<script type="text/javascript">
			alert("ID produk sudah pernah ada");
			window.location=history.go(-1);
			</script>
			<?php
			}
			else{
				$this->produk_model->insert($data)
				?>
				<script>
					alert("Data Berhasil Ditambah");
				</script>
				<?php
				redirect(base_url('produk/tambah_produk'),'refresh');
			}
		}
	}

	function hapus_produk(){
		$id_produk=$this->uri->segment(3);
		if($id_produk=='')redirect(base_url('produk/daftar_produk'));
		$hapus=$this->produk_model->hapus_produk($id_produk);
		?>
		<script>
			window.alert("Data Berhasil Dihapus");
		</script>
		<?php
		redirect(base_url('produk/daftar_produk'),'refresh');
	}

	function edit_produk(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login==false)redirect(base_url('auth'));
		
		$id_detail_produk=$this->uri->segment(3);
		if($id_detail_produk=='')redirect(base_url('produk/daftar_produk'));
		$data['title']='Edit Produk';
		$data['content']='edit_produk';
		$data['produk']=$this->produk_model->select_produk_by_id($id_detail_produk);
		$data['kategori_produk']=$this->produk_model->select_all_produk();
		$this->load->view('template',$data);
	}

	function proses_edit_produk(){
		$id_detail_produk=$this->input->post('id_detail_produk');
		$data['id_detail_produk']=$this->input->post('id_detail_produk_baru');
		$data['jenis_produk']=$this->input->post('jenis_produk');
		$data['harga_produk']=$this->input->post('harga_produk');
		$data['keterangan']=$this->input->post('keterangan');
		$data['id_produk']=$this->input->post('id_produk');
		if($this->input->post('hpp')) {
			$data['hpp']=$this->input->post('hpp');
			$this->form_validation->set_rules('hpp','hpp','required');
		}
		$this->form_validation->set_rules('jenis_produk','jenis_produk','required');
		$this->form_validation->set_rules('harga_produk','harga_produk','required');
		$this->form_validation->set_rules('id_produk','id_produk','required');
		if($this->form_validation->run() == FALSE){
			?>
				<script>
					alert("Form tidak boleh ada yg kosong");
					window.location=history.go(-1);
				</script>
			<?php
		}
		else{
			$this->produk_model->edit_produk($id_detail_produk,$data);
			?>
			<script>
				alert("Data Berhasil Diubah");
			</script>
			<?php
			redirect(base_url('produk/daftar_produk'),'refresh');
		}
	}

	function daftar_master_produk(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($login==false)redirect(base_url('auth'));

		
		$data['title']='Master Produk';
		$data['content']='daftar_master_produk';
		$data['master_produk']=$this->produk_model->select_all_produk();
		$this->load->view('template',$data);
	}

	public function json_kategori_produk(){
		header('Content-Type: application/json');
        echo $this->produk_model->json_kategori_produk();
	}

	function tambah_master_produk(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($login==false)redirect(base_url('auth'));
		
		$data['title']='Tambah Master Produk';
		$data['content']='tambah_master_produk';
		$this->load->view('template',$data);
	}

	function proses_tambah_master_produk(){
		$data['id_produk']=$this->input->post('id_produk');
		$data['nama_produk']=$this->input->post('nama_produk');
		$this->form_validation->set_rules('id_produk','id_produk','required');
		$this->form_validation->set_rules('nama_produk','nama_produk','required');
		$cek_master_produk=$this->produk_model->select_master_produk_by_id($this->input->post('id_produk'));
		
		if($this->form_validation->run() == FALSE){
			?>
				<script>
					alert("Form tidak boleh ada yg kosong");
					window.location=history.go(-1);
				</script>
			<?php
		}
		else{
			if ($cek_master_produk->num_rows()==1) {
				# code...
			?>
			<script type="text/javascript">
				alert("ID Produk Sudah Pernah Ada");
				window.location=history.go(-1);
			</script>
			<?php
			}
			else{
				$this->produk_model->insert_master_produk($data)
				?>
				<script>
					alert("Data Berhasil Ditambah");
				</script>
				<?php
				redirect(base_url('produk/tambah_master_produk'),'refresh');
			}
		}
	}

	function hapus_master_produk(){
		$id_produk=$this->uri->segment(3);
		if($id_detail_produk=='')redirect(base_url('produk/daftar_master_produk'));
		$hapus=$this->produk_model->hapus_master_produk($id_produk);
		if ($hapus) {
			# code...
		?>
		<script>
			alert("Data Berhasil Dihapus");
		</script>
		<?php
		}
		else{
		?>
		<script>
			alert("Data Gagal Dihapus");
		</script>
		<?php
		redirect(base_url('produk/daftar_master_produk'),'refresh');
		}
	}

	function edit_master_produk(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		if($login==false)redirect(base_url('auth'));
		
		$id_produk=$this->uri->segment(3);
		if($id_produk=='')redirect(base_url('produk/daftar_master_produk'));
		$data['title']='Edit Master Produk';
		$data['content']='edit_master_produk';
		$data['master_produk']=$this->produk_model->select_master_produk_by_id($id_produk);
		$this->load->view('template',$data);
	}

	function proses_edit_master_produk(){
		$id_produk=$this->input->post('id_produk');
		$id_produk2=$this->input->post('id_produk2');
		$data['id_produk']=$this->input->post('id_produk2');
		$data['nama_produk']=$this->input->post('nama_produk');
		$this->form_validation->set_rules('id_produk2','id_produk2','required');
		$this->form_validation->set_rules('nama_produk','nama_produk','required');
		if($this->form_validation->run() == FALSE){
			?>
				<script>
					alert("Form tidak boleh ada yg kosong");
					window.location=history.go(-1);
				</script>
			<?php
		}
		else{
			$this->produk_model->edit_master_produk($id_produk,$data);
			?>
			<script>
				alert("Data Berhasil Diubah");
			</script>
			<?php
			redirect(base_url('produk/daftar_master_produk'),'refresh');
		}
	}	
}
