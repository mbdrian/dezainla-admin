<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model(array('auth_model','order_model','produk_model','user_model'));
		$this->load->library(array('session','form_validation','encryption','datatables'));
		$this->load->helper(array('url','form','download'));
		date_default_timezone_set('Asia/Jakarta');
	}

	function index(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='admin' || $level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Data Pegawai';
		$data['user']=$this->user_model->select_all_user();
		$data['content']='daftar_user';
		$this->load->view('template',$data);
	}

	function json(){
		header('Content-Type: application/json');
        echo $this->user_model->json();
	}

	function rekapitulasi_order_user(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level!='ceo')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Rekapitulasi Order Berdasarkan User';
		$data['user']=$this->user_model->select_all_user();
		$data['content']='rekapitulasi_order_user';
		$this->load->view('template',$data);
	}

	function proses_rekapitulasi_order_user(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level!='ceo')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));

		$id_user=$this->input->post('nama_pegawai');
		$tanggal_mulai=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
		$tanggal_selesai=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
		$this->form_validation->set_rules('nama_pegawai','nama_pegawai','required');
		$this->form_validation->set_rules('tanggal_mulai','tanggal_mulai','required');
		$this->form_validation->set_rules('tanggal_selesai','tanggal_selesai','required');
		if($this->form_validation->run() == true){
			$data['id_user']=$this->input->post('nama_pegawai');
			$data['tanggal_mulai']=date('Y-m-d',strtotime($this->input->post('tanggal_mulai')));
			$data['tanggal_selesai']=date('Y-m-d',strtotime($this->input->post('tanggal_selesai')));
			$data['user']=$this->user_model->select_all_user();
			$data['user_pilih']=$this->user_model->rekapitulasi_order_user($id_user,$tanggal_mulai,$tanggal_selesai);
			$data['title']='Rekapitulasi Order Berdasarkan User';
			$data['content']='rekapitulasi_order_user';
			$this->load->view('template',$data);
		}
		else{
			$this->session->set_flashdata('status', '<div class="alert alert-danger" role="alert">Maaf, form harus diisi</div>');

			redirect(base_url('user/rekapitulasi_order_user'));
		}
	}

	function hapus_user(){
		$id_user=$this->uri->segment(3);
		$this->user_model->hapus_user($id_user);
		$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">User berhasil dihapus</div>');
		redirect(base_url('user'),'refresh');
	}

	function tambah_user(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='admin' || $level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Tambah Pegawai';
		$data['content']='tambah_user';
		$this->load->view('template',$data);	
	}

	function proses_tambah_user(){
		$data['username']			= $this->input->post('username');
		$data['password']			= md5(trim(strip_tags($this->input->post('password'))));
		$data['nama']			= $this->input->post('nama');
		$data['level']			= $this->input->post('level');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		$this->form_validation->set_rules('nama','nama','required');
		$this->form_validation->set_rules('level','level','required');
		if($this->form_validation->run() == FALSE)
		{
		?>
		<script type="text/javascript">
		window.location=history.go(-1);
		</script>
		<?php
		}
		else
		{
			$this->user_model->insert($data);
			$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Data user berhasil ditambahkan</div>');
			redirect(base_url('user'), 'refresh');
		}

	}

	function edit_user(){
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='admin' || $level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		
		$id_user=$this->uri->segment(3);
		if($id_user=='')redirect(base_url('user'));
		$data['title']='Edit User';
		$data['content']='edit_user';
		$data['user']=$this->user_model->select_user_by_id($id_user);
		$this->load->view('template',$data);
	}

	public function detail_user(){
		$id_user=$this->input->post('id');
		$data['user']=$this->user_model->select_user_by_id($id_user);
		$this->load->view('page/detail_user',$data);
	}

	function proses_edit_user(){
		$id_user 					= $this->input->post('id_user');
		$data['username']			= $this->input->post('username');
		$data['password']			= md5(trim(strip_tags($this->input->post('password'))));
		$data['nama']			= $this->input->post('nama');
		$data['level']			= $this->input->post('level');
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('nama','nama','required');
		$this->form_validation->set_rules('level','level','required');
		if($this->form_validation->run() == FALSE)
		{
		?>
		<script type="text/javascript">
		window.location=history.go(-1);
		</script>
		<?php
		}
		else
		{
			$this->user_model->update_user($data,$id_user);
			$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Data user berhasil diubah</div>');			
			redirect(base_url('user'), 'refresh');
		}
	}
	
	public function ganti_password(){	
		$data['username'] = $this->session->userdata('username');
		$data['level'] = $this->session->userdata('level');
		$data['id_user'] = $this->session->userdata('id_user');
		$login=$this->session->userdata('login');
		$level=$this->session->userdata('level');
		if($level=='admin' || $level=='produksi')redirect(base_url('dashboard'));
		if($login==false)redirect(base_url('auth'));
		$data['title']='Ganti Password Pengguna';
		$data['content']='password_user';
		$this->load->view('template',$data);
	}

	function update_password_user(){
		$id							= $this->input->post('id');
		$data['password']			= md5(trim(strip_tags($this->input->post('password'))));
		$temp_password				= md5(trim(strip_tags($this->input->post('temp_password'))));
		$this->form_validation->set_rules('password','Password','required|min_length[6]');
		$this->form_validation->set_rules('temp_password','Password','required|min_length[6]');
		if ($data['password'] != $temp_password)
		{
			$this->session->set_flashdata('status', '<div class="alert alert-danger" role="alert">Password tidak cocok</div>');						
			$this->ganti_password();
		}
		if($this->form_validation->run() == FALSE)
		{
			$this->ganti_password();
		}
		else
		{
			$this->user_model->update_password_user($data,$id);
			$this->session->set_flashdata('status', '<div class="alert alert-success" role="alert">Password berhasil diubah, silahkan login kembali</div>');						
			redirect(base_url('auth/logout'), 'refresh');
		}
	}

}
?>
