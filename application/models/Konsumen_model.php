<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class konsumen_model extends CI_Model {
	
	function insert($konsumen){
		$this->db->insert('konsumen',$konsumen);
	}

	function select_all_konsumen($nama_konsumen){
		$this->db->select('*');
		$this->db->from('konsumen');
		$this->db->where('nama_konsumen',$nama_konsumen);

		return $this->db->get();
	}

	function select(){
		$this->db->select('*');
		$this->db->select('(SELECT COUNT(*) FROM order_produk B WHERE B.nama_konsumen=A.nama_konsumen) as total_order');
		$this->db->from('konsumen A');

		return $this->db->get();
	}

	function hapus_konsumen($nama_konsumen) {
		$query=$this->db->query("DELETE FROM konsumen WHERE nama_konsumen='$nama_konsumen'");
	}

	function update($nama_konsumen, $data) {
		$this->db->where('nama_konsumen', $nama_konsumen);
		$this->db->update('konsumen', $data);

		// return $this->db->get();
	}
}
