<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {
	public function select_all_order()
	{
		$query=$this->db->query("SELECT * FROM order_produk");

		return $query;
	}

	function select_order_by_id_etail($id_detail_order){
		$this->db->select('*');
		$this->db->from('order_produk');
		$this->db->join('detail_produk','detail_produk.id_detail_produk=order_produk.id_detail_produk');
		$this->db->where('order_produk.id_detail_order',$id_detail_order);

		return $this->db->get();
	}


	function update_data_order($id_detail_order,$data){
		$this->db->where('id_detail_order',$id_detail_order);
		$this->db->update('order_produk',$data);
	}

	function update_detail_order($id_detail_order,$data){
		$this->db->where('id_detail_order',$id_detail_order);
		$this->db->update('detail_order',$data);
	}

	function select_order_by_nama($nama){
		$query=$this->db->query("SELECT * FROM order_produk WHERE nama_konsumen LIKE '%$nama%' GROUP BY nama_konsumen");

		return $query;
	}

	function select_detail_pembayaran($id_order){
		$query=$this->db->query("SELECT * FROM detail_order INNER JOIN order_produk ON detail_order.id_detail_order=order_produk.id_detail_order WHERE detail_order.id_detail_order='$id_order' GROUP BY order_produk.id_detail_order");

		return $query;
	}

	function select_info_user($id_detail_order){
		$query=$this->db->query("SELECT order_produk.id_detail_order,order_produk.tanggal_order,order_produk.nama_konsumen,order_produk.hp_konsumen,order_produk.tanggal_pengambilan,order_produk.waktu_pengambilan,user.nama FROM order_produk INNER JOIN user ON order_produk.id_user=user.id_user WHERE id_detail_order='$id_detail_order' ");

		return $query;
	}

	function select_info_user_detail($id_detail_order){
		$query=$this->db->query("SELECT order_produk.id_detail_order,order_produk.tanggal_order,order_produk.nama_konsumen,order_produk.hp_konsumen,order_produk.tanggal_pengambilan,order_produk.waktu_pengambilan,user.nama FROM order_produk INNER JOIN user ON order_produk.id_user=user.id_user WHERE id_detail_order='$id_detail_order' GROUP BY id_detail_order");

		return $query;
	}

	function delete_order_produk($id_order_produk){
		$this->db->where('id_order_produk',$id_order_produk);
		$this->db->delete('order_produk');
	}

	function tracking_detail_by_id($id_order){
		$query=$this->db->query("SELECT order_produk.harga_akhir,detail_produk.id_detail_produk,detail_produk.harga_produk,detail_produk.jenis_produk,order_produk.kuantitas,order_produk.panjang,order_produk.lebar,order_produk.subtotal,tracking_order.status FROM order_produk INNER JOIN detail_produk ON detail_produk.id_detail_produk=order_produk.id_detail_produk INNER JOIN tracking_order ON tracking_order.id_order_produk=order_produk.id_order_produk WHERE order_produk.id_detail_order='$id_order' GROUP BY order_produk.id_order_produk");

		return $query;
	}

	function set_status_pembayaran($id_order,$data){
		$this->db->where('id_detail_order',$id_order);
		$this->db->update('detail_order',$data);
	}

	function insert_order($data){
		$this->db->insert('order_produk',$data);
	}

	function insert_detail_order($detail){
		$this->db->insert('detail_order',$detail);
	}

	function select_all_order_by_date($tanggal){
		$query=$this->db->query("SELECT * FROm order_produk INNER JOIN detail_order ON order_produk.id_order=detail_order.id_order WHERE order_produk.tanggal_order='$tanggal' GROUP BY order_produk.id_order");

		return $query;
	}

	function select_all_order_by_daterange($tangal_mulai,$tanggal_selesai){

		$this->db->select('order_produk.id_detail_order, order_produk.id_order_produk, order_produk.id_user, order_produk.id_detail_produk, nama_konsumen, 
							hp_konsumen, tanggal_order, tanggal_pengambilan, waktu_pengambilan, panjang, lebar, 
							kuantitas, harga_akhir, subtotal, id_produk, jenis_produk, harga_produk, 
							detail_produk.keterangan as nama_produk, total_bayar, uang_muka, diskon, sisa_pembayaran, nama,
							status_pembayaran, status_pengambilan, detail_order.keterangan');
		$this->db->select('(SELECT SUM(panjang * lebar * kuantitas * hpp) FROM order_produk a INNER JOIN
							detail_produk b ON a.id_detail_produk = b.id_detail_produk WHERE
							a.id_detail_order = detail_order.id_detail_order) AS hpp');
		$this->db->from('order_produk');
		$this->db->join('detail_order', 'order_produk.id_detail_order = detail_order.id_detail_order', 'inner');
		$this->db->join('detail_produk', 'order_produk.id_detail_produk = detail_produk.id_detail_produk', 'inner');
		$this->db->join('user', 'order_produk.id_user = user.id_user', 'left');
		$this->db->where("order_produk.tanggal_order BETWEEN '$tangal_mulai' AND '$tanggal_selesai'");
		$this->db->order_by('order_produk.tanggal_order', 'asc');
		return $this->db->get();
	}

	function select_all_keuangan_by_daterange($tangal_mulai,$tanggal_selesai){
		$query=$this->db->query("SELECT *,
										(SELECT 
												SUM(panjang * lebar * kuantitas * hpp)
											FROM
												order_produk a
													INNER JOIN
												detail_produk b ON a.id_detail_produk = b.id_detail_produk
											WHERE
												a.id_detail_order = detail_order.id_detail_order) AS hpp
								FROM order_produk 
								INNER JOIN detail_order ON order_produk.id_detail_order = detail_order.id_detail_order
								INNER JOIN detail_produk ON order_produk.id_detail_produk = detail_produk.id_detail_produk
								LEFT JOIN user ON order_produk.id_user = user.id_user
								WHERE
									order_produk.tanggal_order BETWEEN '$tangal_mulai' AND '$tanggal_selesai'
								GROUP BY detail_order.id_detail_order
								ORDER BY order_produk.tanggal_order ASC");
		
		return $query;
	}

	function select_all_order_by_user($tanggal_mulai,$tanggal_selesai,$username){
		$query=$this->db->query("SELECT order_produk.id_order_produk,detail_order.id_detail_order,detail_produk.jenis_produk,order_produk.tanggal_order,order_produk.tanggal_pengambilan,tracking_order.status FROM order_produk INNER JOIN detail_produk ON detail_produk.id_detail_produk=order_produk.id_detail_produk INNER JOIN tracking_order ON order_produk.id_order_produk=tracking_order.id_order_produk JOIN detail_order ON detail_order.id_detail_order=order_produk.id_detail_order WHERE order_produk.id_user='$username' AND order_produk.tanggal_order BETWEEN '$tanggal_mulai' AND '$tanggal_selesai' GROUP BY order_produk.id_order_produk");
		//$this->db->select('order_produk.id_order_produk','detail_order.id_detail_order','detail_produk.jenis_produk','order_produk.tanggal_order','order_produk.tanggal_pengambilan','tracking_order.status')
		
		return $query;
	}

	function select_all_order_by_status(){
		$query=$this->db->query("SELECT user.nama,order_produk.id_order_produk,concat(datediff(current_date(),tanggal_pengambilan),' hari') as selisih,order_produk.id_detail_order,order_produk.nama_konsumen,order_produk.hp_konsumen,order_produk.tanggal_order,order_produk.tanggal_pengambilan,detail_order.status_pengambilan FROM detail_order JOIN order_produk ON detail_order.id_detail_order=order_produk.id_detail_order INNER JOIN user ON user.id_user=order_produk.id_user GROUP BY id_detail_order ORDER BY order_produk.tanggal_order DESC");

		return $query;
	}

	function json(){
		$this->datatables->select('user.nama,order_produk.id_order_produk,concat(datediff(current_date(),tanggal_pengambilan)," hari") as selisih,order_produk.id_detail_order,order_produk.nama_konsumen,order_produk.hp_konsumen,date_format(order_produk.tanggal_order,"%d-%m-%Y") as tanggal_order,date_format(order_produk.tanggal_pengambilan,"%d-%m-%Y") as tanggal_pengambilan,detail_order.status_pengambilan');
        $this->datatables->from('detail_order');
        $this->datatables->join('order_produk','detail_order.id_detail_order=order_produk.id_detail_order');
        $this->datatables->join('user', 'user.id_user=order_produk.id_user');
        $this->datatables->group_by('detail_order.id_detail_order');
        $this->datatables->order_by('order_produk.tanggal_order','desc');
        $this->datatables->add_column('action', '<a href="#" class="edit-record" data-id="$1"><i class="fa fa-search-plus"></i> Detail</a>','id_detail_order');
        $this->datatables->add_column('yuk', '<a href="'.base_url().'order/edit_data_order/$1"><i class="fa fa-edit"></i></a> <a href="'.base_url().'cetak/faktur_order/$1" target="_blank"><i class="fa fa-print"></i></a>','id_detail_order');
        $this->datatables->add_column('id_order','<a href="'.base_url().'order/hapus_data_order/$1/$2" onclick="return konfirmasi()"><i class="fa fa-trash"></i> Hapus</a>','id_order_produk,id_detail_order');
        return $this->datatables->generate();
	}

	function jsonwithParams($year, $month) {
		$this->datatables->select('user.nama,order_produk.id_order_produk,concat(datediff(current_date(),tanggal_pengambilan)," hari") as selisih,order_produk.id_detail_order,order_produk.nama_konsumen,order_produk.hp_konsumen,date_format(order_produk.tanggal_order,"%d-%m-%Y") as tanggal_order,date_format(order_produk.tanggal_pengambilan,"%d-%m-%Y") as tanggal_pengambilan,detail_order.status_pengambilan');
        $this->datatables->from('detail_order');
        $this->datatables->join('order_produk','detail_order.id_detail_order=order_produk.id_detail_order');
		$this->datatables->join('user', 'user.id_user=order_produk.id_user');
		if ($month!="") $this->datatables->where('extract(month from order_produk.tanggal_order)=', $month);
		if ($year!="") $this->datatables->where('extract(year from order_produk.tanggal_order)=', $year);
        $this->datatables->group_by('detail_order.id_detail_order');
        $this->datatables->order_by('order_produk.tanggal_order','desc');
        $this->datatables->add_column('action', '<a href="#" class="edit-record" data-id="$1"><i class="fa fa-search-plus"></i> Detail</a>','id_detail_order');
        $this->datatables->add_column('yuk', '<a href="'.base_url().'order/edit_data_order/$1"><i class="fa fa-edit"></i></a> <a href="'.base_url().'cetak/faktur_order/$1" target="_blank"><i class="fa fa-print"></i></a>','id_detail_order');
        $this->datatables->add_column('id_order','<a href="'.base_url().'order/hapus_data_order/$1/$2" onclick="return konfirmasi()"><i class="fa fa-trash"></i> Hapus</a>','id_order_produk,id_detail_order');
        return $this->datatables->generate();
	}

	function json_tracking_deadline(){
		$this->datatables->select('user.nama,order_produk.id_detail_order,order_produk.nama_konsumen,order_produk.hp_konsumen,
								order_produk.tanggal_order,order_produk.tanggal_pengambilan,detail_order.status_pengambilan, 
								concat(datediff(tanggal_pengambilan,current_date())," hari") as selisih');
        $this->datatables->from('detail_order');
        $this->datatables->join('order_produk','detail_order.id_detail_order=order_produk.id_detail_order');
        $this->datatables->join('tracking_order', 'tracking_order.id_order_produk=order_produk.id_order_produk');
        $this->datatables->join('user', 'order_produk.id_user=user.id_user');
        $this->datatables->where('tracking_order.status', 'dalam proses');
        $this->datatables->where('datediff(current_date(),tanggal_pengambilan) <=',0);
        $this->datatables->group_by('order_produk.id_detail_order');
        $this->datatables->order_by('cast(selisih as signed)','asc');
        $this->datatables->add_column('action', '<a href="#" class="edit-record" data-id="$1"><i class="fa fa-edit"></i> Detail</a>','id_detail_order');

        return $this->datatables->generate();
	}

	function tracking_deadline(){
		$query=$this->db->query("SELECT user.nama,order_produk.id_detail_order,order_produk.nama_konsumen,order_produk.hp_konsumen,
								order_produk.tanggal_order,order_produk.tanggal_pengambilan,detail_order.status_pengambilan, 
								concat(datediff(tanggal_pengambilan,current_date()),' hari') as selisih 
								FROM detail_order 
								JOIN order_produk ON detail_order.id_detail_order=order_produk.id_detail_order 
								JOIN tracking_order ON tracking_order.id_order_produk=order_produk.id_order_produk 
								JOIN user ON order_produk.id_user=user.id_user
								WHERE tracking_order.status='dalam proses' 
								AND datediff(current_date(),tanggal_pengambilan) <= 0 
								GROUP BY order_produk.id_detail_order 
								ORDER BY selisih asc");

		return $query;
	}

	function tracking_deadline_lewat(){
		$query=$this->db->query("SELECT user.nama,order_produk.id_detail_order,order_produk.nama_konsumen,order_produk.hp_konsumen,
								order_produk.tanggal_order,order_produk.tanggal_pengambilan,detail_order.status_pengambilan, 
								datediff(current_date(),tanggal_pengambilan) as selisih 
								FROM detail_order 
								JOIN order_produk ON detail_order.id_detail_order=order_produk.id_detail_order 
								JOIN tracking_order ON tracking_order.id_order_produk=order_produk.id_order_produk 
								JOIN user ON order_produk.id_user=user.id_user
								WHERE tracking_order.status='dalam proses' AND datediff(current_date(),tanggal_pengambilan) > 0 
								GROUP BY order_produk.id_detail_order 
								ORDER BY selisih ASC");

		return $query;
	}

	function json_tracking_deadline_lewat(){
		$this->datatables->select('user.nama,order_produk.id_detail_order,order_produk.nama_konsumen,order_produk.hp_konsumen,
								order_produk.tanggal_order,order_produk.tanggal_pengambilan,detail_order.status_pengambilan, 
								concat(datediff(current_date(),tanggal_pengambilan)," hari") as selisih');
        $this->datatables->from('detail_order');
        $this->datatables->join('order_produk','detail_order.id_detail_order=order_produk.id_detail_order');
        $this->datatables->join('tracking_order', 'tracking_order.id_order_produk=order_produk.id_order_produk');
        $this->datatables->join('user', 'order_produk.id_user=user.id_user');
        $this->datatables->where('tracking_order.status','dalam proses');
        $this->datatables->where('datediff(current_date(),tanggal_pengambilan) >',0);
        $this->datatables->where('detail_order.status_pengambilan','belum_diambil');
        $this->datatables->group_by('order_produk.id_detail_order');
        $this->datatables->order_by('cast(selisih as signed)','asc');
        $this->datatables->add_column('action', '<a href="'.base_url().'order/edit_tracking_deadline_lewat/$1"><i class="fa fa-edit"></i></a>','id_detail_order');

        return $this->datatables->generate();
	}

	function delete_detail_order($id_detail_order){
		$this->db->where('id_detail_order',$id_detail_order);
		$this->db->delete('detail_order');
	}

	function delete_order($id_detail_order){
		$this->db->where('id_detail_order',$id_detail_order);
		$this->db->delete('order_produk');
	}


	function edit_tracking_deadline_lewat($id_detail_order){
		$query=$this->db->query("SELECT order_produk.id_detail_order,order_produk.tanggal_pengambilan,detail_produk.id_detail_produk,detail_produk.harga_produk,detail_produk.jenis_produk,order_produk.kuantitas,order_produk.panjang,order_produk.lebar,order_produk.subtotal,tracking_order.status 
								FROM order_produk 
								INNER JOIN detail_produk ON detail_produk.id_detail_produk=order_produk.id_detail_produk 
								INNER JOIN tracking_order ON tracking_order.id_order_produk=order_produk.id_order_produk 
								WHERE order_produk.id_detail_order='$id_detail_order'
								");
	
		return $query;
	}

	function proses_tanggal_deadline_baru($id_detail_order,$data){
		$this->db->where('id_detail_order',$id_detail_order);
		$this->db->update('order_produk',$data);
	}

	function set_tracking_order_produk($id_order_produk){
		$query=$this->db->query("SELECT * FROM order_produk INNER JOIN detail_produk ON detail_produk.id_detail_produk=order_produk.id_detail_produk INNER JOIN tracking_order ON order_produk.id_order_produk=tracking_order.id_order_produk where order_produk.id_order_produk='$id_order_produk'");
	
		return $query;
	}

	function proses_set_tracking_order($id_order_produk,$data){
		$this->db->where('id_order_produk',$id_order_produk);
		$this->db->update('tracking_order',$data);
	}

	function json_tracking_order(){
		$this->datatables->select('order_produk.nama_konsumen,order_produk.hp_konsumen,concat(datediff(current_date(),tanggal_pengambilan)," hari") as selisih,order_produk.id_order_produk,detail_order.id_detail_order,detail_produk.jenis_produk,order_produk.tanggal_order,order_produk.tanggal_pengambilan,tracking_order.status');
        $this->datatables->from('order_produk');
        $this->datatables->join('detail_produk','detail_produk.id_detail_produk=order_produk.id_detail_produk');
        $this->datatables->join('tracking_order', 'order_produk.id_order_produk=tracking_order.id_order_produk');
        $this->datatables->join('detail_order', 'detail_order.id_detail_order=order_produk.id_detail_order');
        $this->datatables->where('tracking_order.status !=','selesai');
        $this->datatables->group_by('order_produk.id_order_produk');
        $this->datatables->order_by('order_produk.tanggal_order','desc');
        $this->datatables->order_by('cast(selisih as signed)','asc');
        $this->datatables->add_column('action', '<a href="#" class="edit-record" data-id="$1"><i class="fa fa-edit"></i></a>','id_order_produk');
        return $this->datatables->generate();
	}

	function tracking_order_status(){
		$query=$this->db->query("SELECT order_produk.nama_konsumen,order_produk.hp_konsumen, datediff(current_date(),tanggal_pengambilan) as selisih,order_produk.id_order_produk,detail_order.id_detail_order,detail_produk.jenis_produk,order_produk.tanggal_order,order_produk.tanggal_pengambilan,tracking_order.status FROM order_produk INNER JOIN detail_produk ON detail_produk.id_detail_produk=order_produk.id_detail_produk INNER JOIN tracking_order ON order_produk.id_order_produk=tracking_order.id_order_produk JOIN detail_order ON detail_order.id_detail_order=order_produk.id_detail_order WHERE tracking_order.status!='selesai' GROUP BY order_produk.id_order_produk");

		return $query;
	}

	function select_detail_order_by_id($id_detail_order){
		$this->db->select('*');
		$this->db->from('detail_order');
		$this->db->where('id_detail_order',$id_detail_order);

		return $this->db->get();
	}

	function status_pengerjaan($id_order){
		$query=$this->db->query("SELECT order_produk.id_order_produk,detail_order.id_detail_order,detail_produk.jenis_produk,order_produk.tanggal_order,order_produk.tanggal_pengambilan,tracking_order.status FROM order_produk INNER JOIN detail_produk ON detail_produk.id_detail_produk=order_produk.id_detail_produk INNER JOIN tracking_order ON order_produk.id_order_produk=tracking_order.id_order_produk INNER JOIN detail_order ON detail_order.id_detail_order=order_produk.id_detail_order WHERE order_produk.id_order_produk='$id_order' GROUP BY order_produk.id_order_produk");

		return $query;
	}

	function select_count_order(){
		$query=$this->db->query("select sum(detail_order.uang_muka) as uang_muka, extract(month from tanggal_order) as bulan from order_produk inner join detail_order on detail_order.id_detail_order=order_produk.id_detail_order  group by extract(month from tanggal_order)");

		return $query;
	}

	function select_count_order_by_date($tangal_mulai,$tanggal_selesai){
		$query=$this->db->query("SELECT count(id_order_produk) as rekap_order, tanggal_order FROM order_produk WHERE tanggal_order BETWEEN '$tangal_mulai' AND '$tanggal_selesai' GROUP BY tanggal_order");

		return $query;
	}

	function select_count_order_by_year($year) {
		$query=$this->db->query("select sum(detail_order.uang_muka) as uang_muka, 
		extract(month from tanggal_order) as bulan 
		from order_produk inner join detail_order on detail_order.id_detail_order=order_produk.id_detail_order 
		where extract(year from tanggal_order) = '".$year."'
		group by extract(month from tanggal_order)");

		return $query;
	}

	function select_count_konsumen(){
		$query=$this->db->query("select extract(month from tanggal_order) as bulan,
		count(order_produk.nama_konsumen) as juml_konsumen from order_produk 
        group by extract(month from tanggal_order)");

		return $query;
	}

	function select_count_konsumen_by_date($tangal_mulai,$tanggal_selesai){
		$query=$this->db->query("SELECT count(DISTINCT nama_konsumen) as rekap_konsumen, tanggal_order FROM order_produk WHERE tanggal_order BETWEEN '$tangal_mulai' AND '$tanggal_selesai' GROUP BY tanggal_order");

		return $query;
	}

	function select_count_konsumen_by_year($year){
		$query=$this->db->query("select extract(month from tanggal_order) as bulan,
		count(order_produk.nama_konsumen) as juml_konsumen from order_produk 
        where extract(year from tanggal_order)='".$year."'
        group by extract(month from tanggal_order)");

		return $query;
	}

	function select_count_all_konsumen(){
		$query=$this->db->query("SELECT DISTINCT A.nama_konsumen FROM konsumen A");

		return $query;
	}

	function select_order_by_id($id_detail_order){
		$this->db->select('*');
		$this->db->from('order_produk');
		$this->db->where('id_detail_order',$id_detail_order);
		$this->db->group_by('id_detail_order');


		return $this->db->get();
	}

	function edit_detail_order($id_detail_order){
		$this->db->select('*');
		$this->db->from('order_produk');
		$this->db->join('detail_produk','detail_produk.id_detail_produk=order_produk.id_detail_produk');
		$this->db->where('id_detail_order',$id_detail_order);
		return $this->db->get();
	}

	function edit_detail_order_produk($id_detail_order){
		$this->db->select('*');
		$this->db->from('detail_order');
		$this->db->where('id_detail_order',$id_detail_order);
		return $this->db->get();
	}
}
