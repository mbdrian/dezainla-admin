<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk_model extends CI_Model {
	public function __construct() {
        parent::__construct();
    }

    function json() {
        $this->datatables->select('id_detail_produk,jenis_produk,harga_produk,keterangan,produk.nama_produk as kategori_produk');
        $this->datatables->from('detail_produk');
        $this->datatables->join('produk', 'produk.id_produk = detail_produk.id_produk');
        $this->datatables->add_column('action', '<a href="'.base_url().'produk/edit_produk/$1"><i class="fa fa-edit"></i></a> <a href="'.base_url().'produk/hapus_produk/$1"><i class="fa fa-trash"></i></a>','id_detail_produk');
        return $this->datatables->generate();
    }


    function json_kategori_produk() {
        $this->datatables->select('id_produk,nama_produk');
        $this->datatables->from('produk');
        $this->datatables->add_column('action', '<a href="'.base_url().'produk/edit_master_produk/$1"><i class="fa fa-edit"></i></a> <a onclick="konfirmasi()" href="'.base_url().'produk/hapus_master_produk/$1"><i class="fa fa-trash"></i></a>','id_produk');
        return $this->datatables->generate();
    }


	public function select_all_produk()
	{
		$query=$this->db->query("SELECT * FROM produk ORDER BY id_produk");

		return $query;
	}

	public function select_all_detail_produk(){
		$this->db->select('*');
		$this->db->from('detail_produk');
		$this->db->join('produk','produk.id_produk=detail_produk.id_produk');
		$this->db->order_by('id_detail_produk');
		
		return $this->db->get();
	}

	function select_produk_by_id($id_detail_produk){
		$this->db->select('*');
		$this->db->from('detail_produk');
		$this->db->join('produk','produk.id_produk=detail_produk.id_produk');
		$this->db->where('detail_produk.id_detail_produk',$id_detail_produk);
		$this->db->order_by('id_detail_produk');
		
		return $this->db->get();
	}

	function select_produk_by_nama($nama){
		$query=$this->db->query("SELECT * FROM detail_produk WHERE jenis_produk LIKE '%$id_detail_produk%'");

		return $query;
	}

	function select_master_produk_by_id($id_produk){
		$query=$this->db->query("SELECT * FROM produk WHERE id_produk='$id_produk'");

		return $query;	
	}	

	function insert($data){
		$this->db->insert('detail_produk',$data);
	}

	function insert_master_produk($data){
		$this->db->insert('produk',$data);	
	}

	function hapus_produk($id_detail_produk){
		$query=$this->db->query("DELETE FROM detail_produk WHERE id_detail_produk='$id_detail_produk'");
	}

	function hapus_master_produk($id_produk){
		$query=$this->db->query("DELETE FROM produk WHERE id_produk='$id_produk'");
	}	

	function edit_produk($id_detail_produk,$data){
		$this->db->where('id_detail_produk',$id_detail_produk);
		$this->db->update('detail_produk',$data);
	}

	function edit_master_produk($id_produk,$data){
		$this->db->where('id_produk',$id_produk);
		$this->db->update('produk',$data);
	}

	public function cari_judul($kode){
  		$this->db->like('jenis_produk',$kode);
  		$query=$this->db->get('detail_produk');
  		return $query->result();
 	}

 	public function getData($bahasa)
    {
        $this->db->select('jenis_produk');
        $this->db->like('jenis_produk', $bahasa);
        $query = $this->db->get('detail_produk');
        return $query->result();
    }
}
