<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	function json() {
        $this->datatables->select('id_user,nama,level');
        $this->datatables->from('user');
        $this->datatables->add_column('action', '<a href="#" class="detail-record" data-id="$1"><i class="fa fa-edit"></i></a> <a onclick="konfirmasi()" href="'.base_url().'user/hapus_user/$1"><i class="fa fa-trash"></i></a>','id_user');
        return $this->datatables->generate();
    }

	public function update_password_user($data,$id)
	{
		$this->db->where('id_user',$id);
		$this->db->update('user',$data);
	}

	function select_all_user(){
		$this->db->select('*');
		$this->db->from('user');

		return $this->db->get();
	}

	function select_all_user_by_level(){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('level','desainer');
		
		return $this->db->get();
	}

	function select_user_by_id($id_user){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('id_user',$id_user);

		return $this->db->get();
	}

	function update_user($data,$id_user){
		$this->db->where('id_user',$id_user);
		$this->db->update('user',$data);
	}

	function hapus_user($id_user){
		$query=$this->db->query("DELETE FROM user WHERE id_user='$id_user'");
	}

	function insert($data){
		$this->db->insert('user',$data);
	}

	function rekapitulasi_order_user($id_user,$tanggal_mulai,$tanggal_selesai){
		$this->db->select('*');
		$this->db->from('order_produk');
		$this->db->join('detail_produk','detail_produk.id_detail_produk=order_produk.id_detail_produk');
		$this->db->join('tracking_order','tracking_order.id_order_produk=order_produk.id_order_produk');
		$this->db->join('user','user.id_user=order_produk.id_user');
		$this->db->where('order_produk.id_user',$id_user);
		$this->db->where('order_produk.tanggal_order >=',$tanggal_mulai);
		$this->db->where('order_produk.tanggal_order <=',$tanggal_selesai);
		$this->db->group_by('detail_produk.jenis_produk');

		return $this->db->get();
	}
}
