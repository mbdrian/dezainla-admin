 <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
<div class="box">
  <!-- /.box-header -->
  <div class="box-body">
  <?php echo $this->session->flashdata('status');?>
    <table id="example1" class="table table-bordered table-striped">
      <thead>
        <tr>      
          <th>No</th>
          <th>Nama</th>
          <th>Hp Konsumen</th>
          <th>Total Order</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no=1;
        foreach ($konsumen->result_array() as $user_item) {
          # code...
        ?>
        <tr>    
          <td><?php echo $no++;?></td>    
          <td><?php echo $user_item['nama_konsumen']; ?></td>
          <td><?php echo $user_item['hp_konsumen']; ?></td>
          <td><?php echo $user_item['total_order']; ?></td>
          <td><h5>
          <!--<a href="<?php echo base_url();?>konsumen/edit_konsumen/<?php echo $user_item['nama_konsumen']; ?>">Edit |</a>-->
                <a href="<?php echo base_url(); ?>konsumen/hapus_konsumen/<?php echo $user_item['nama_konsumen']; ?>" onClick="return konfirmasi()">Hapus |</a>
                <?php
                    $user_item['nama_konsumen'] = str_replace(' ','_', $user_item['nama_konsumen']);
                ?>

                <a href="javascript:konsumen_<?php echo $user_item['nama_konsumen']; ?>()">Edit</a></h5> </td>
                 <script>
                    function konsumen_<?php echo $user_item['nama_konsumen']; ?>()
                    {
                        $('#myModal_<?php echo $user_item['nama_konsumen']; ?>').modal('show');
                    }
                </script>
                <!-- Modal -->
                <div class="modal fade" id="myModal_<?php echo $user_item['nama_konsumen']; ?>" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Detail Konsumen</h4>
                            </div>
                            <div class="modal-body">
                            <?php
                                $user_item['nama_konsumen'] = str_replace('_',' ', $user_item['nama_konsumen']);
                            ?>
                               <form class="form-horizontal" method="post" action="<?php echo base_url(); ?>konsumen/edit_konsumen">
                                    <input type="hidden" name="nama_konsumen" value="<?php echo $user_item['nama_konsumen']; ?>">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 control-label">Nama Konsumen</label>
                                        <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="nama" value="<?php echo $user_item['nama_konsumen']; ?>" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-3 control-label">No Hp Konsumen</label>
                                        <div class="col-sm-5">
                                        <input type="text" class="form-control" placeholder="No Hp" name="hp_konsumen" value="<?php echo $user_item['hp_konsumen']; ?>">
                                        </div>
                                    </div>
                                    <div class="box-footer row">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-info">Edit</button>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                            </div>
                        </div>
                    </div>
                </div>
        </tr>
        <?php
        }
        ?>
      </tbody>
    </table>
  </div>
  <!-- /.box-body -->
</div>
<form action="<?php echo base_url(); ?>cetak/excel_konsumen" method="post">
<button typ="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-file-excel-o"></i> Download data konsumen
          </button>
</form>
<!-- <a href="javascript:konsumen_edit()">info lanjut</a>   -->


<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<script>

  $(function () {
    $("#example1").DataTable();
  });

  function konfirmasi(){
    var pilihan=confirm("Apakah Anda yakin ingin menghapus?");
    if (pilihan) {
      return true;
    }
    else{
      return false;
    }
  }
</script>  
      
