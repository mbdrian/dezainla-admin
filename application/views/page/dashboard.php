      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
      <div class="row">
        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="glyphicon glyphicon-shopping-cart"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Order Produk</span>
              <span class="info-box-number"><?php echo $count_total_order; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-inbox"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Katalog Produk</span>
              <span class="info-box-number"><?php echo $count_produk; ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- Edited by rian -->
        <a href='<?php echo base_url() ?>konsumen'>
            <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="glyphicon  glyphicon-user"></i></span>

                <div class="info-box-content">
                <span class="info-box-text">Total Konsumen</span>
                <span class="info-box-number"><?php echo $count_total_konsumen; ?></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            </div>
        </a>
      </div>
      <div class="box-footer">
          
      </div>
    <form class="form-horizontal" action="<?php echo base_url(); ?>dashboard/proses_cari_grafik" method="post">
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-3 control-label">Pilih Tahun</label>
          <div class="col-sm-6">
          <?php
            //get the current year
            $Startyear=date('Y');
            $endYear=$Startyear-5;

            // set start and end year range i.e the start year
            $yearArray = range($Startyear,$endYear);
            ?>
            <!-- here you displaying the dropdown list -->
            <select name="yearDashboard" class="form-control">
                <option value="">Select Year</option>
                <?php
                foreach ($yearArray as $year) {
                    // this allows you to select a particular year
                    echo '<option value="'.$year.'">'.$year.'</option>';
                }
                ?>
            </select>
          </div>
        </div>
      </div>
      
      <div class="col-md-6">
          <button type="submit" class="btn btn-info">Submit</button>
      </div>
    </div>
    </form>
      <div class="row">
        <div class="col-md-12">
            <div id="view" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
            <div id="konsumen" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
        </div>
      </div>
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<script src="<?php echo base_url(); ?>assets/line/js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/line/js/exporting.js"></script>
<script type="text/javascript">
$(function () {
    $('#view').highcharts({
        title: {
            text: 'Data Jumlah Omset Perbulan Tahun <?php echo $year_now;?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"]
        },
        yAxis: {
            title: {
                text: 'Jumlah Omset'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Jumlah Omset ',
            data: [<?php foreach($chart_order->result_array() as $chart_order_item){echo $chart_order_item['uang_muka'].",";} ?>]
        }]
    });
});
</script>

<script type="text/javascript">
$(function () {
    $('#konsumen').highcharts({
        title: {
            text: 'Data Jumlah Konsumen Perbulan di Tahun <?php echo $year_now;?>',
            x: -20 //center
        },
        subtitle: {
            text: '',
            x: -20
        },
        xAxis: {
            categories: ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"]
        },
        yAxis: {
            title: {
                text: 'Jumlah konsumen'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Jumlah Konsumen ',
            data: [<?php foreach($chart_konsumen->result_array() as $chart_konsumen_item){echo $chart_konsumen_item['juml_konsumen'].",";} ?>]
        }]
    });
});
</script>
<script>
//   $('#datepicker').datepicker({
//     format: 'yyyy-dd-mm',
//       autoclose: true
//     });
//   $('#datepicker2').datepicker({
//     format: 'yyyy-dd-mm',
//       autoclose: true
//     });
</script>
