<table class="table table-condensed">
    <tbody><tr>
      <th>No</th>
      <th>ID Produk</th>
      <th>Nama Produk</th>
      <th>Harga</th>
      <th>Ukuran</th>
      <th>Kuantitas</th>
      <th>Subtotal</th>
      <th>Status</th>
    </tr>
    <?php
    $no=1;
    foreach ($detail_order->result() as $row) {
      # code...
    ?>
    <tr>
      <td><?php echo $no++; ?></td>
      <td><?php echo $row->id_detail_produk;  ?></td>
      <td><?php echo $row->jenis_produk;  ?></td>
      <td>Rp. <?php echo number_format($row->harga_akhir, "2", ",", ".");  ?></td>
      <td><?php echo $row->panjang." x ".$row->lebar; ?></td>
      <td><?php echo $row->kuantitas; ?></td>
      <td>Rp. <?php echo number_format($row->subtotal, "2", ",", ".");  ?></td>
      <td><?php echo $row->status;  ?></td>
    </tr>
    <?php
    }
    ?>
    <tr>
      <td colspan="8"></td>
    </tr>
    <tr>
      <td>TOTAL BAYAR</td>
      <td>Rp. 
        <?php
        $total_bayaran = 0;
        foreach ($detail_pembayaran->result() as $value) {
          # code...
        echo number_format($value->total_bayar, "2", ",", ".");
        $total_bayaran += $value->total_bayar;
        }
        ?>
      </td>
      <td></td>
      <td><b>Keterangan</b></td>
    </tr>
    <tr>
      <td>UANG MUKA</td>
      <td>Rp. <span id='uang_muka'>
        <?php
        foreach ($detail_pembayaran->result() as $value) {
          # code...
        echo number_format($value->uang_muka, "2", ",", ".");
        }
        ?></span>
      </td>
      <td></td>
      <?php
      $no=1;
      foreach ($detail_pembayaran->result() as $row) {
        # code...
      ?>
      <td colspan='5' rowspan='4'><?php if($row->keterangan!="") echo $row->keterangan; 
      else echo "Tidak ada keterangan";?></td>
      <?php }?>
    </tr>
    <tr>
      <td>DISKON</td>
      <td>Rp. 
        <?php
        foreach ($detail_pembayaran->result() as $value) {
          # code...
        echo number_format($value->diskon, "2", ",", ".");
        }
        ?>
      </td>
    </tr>
    <tr>
      <td>SISA PEMBAYARAN</td>
      <td>Rp. <span id='sisa_bayar'>
        <?php
        foreach ($detail_pembayaran->result() as $value) {
          # code...
        echo number_format($value->sisa_pembayaran, "2", ",", ".");
        }
        ?></span>
      </td>
    </tr>
    <tr>
      <td>STATUS BAYAR</td>
      <td>
        <?php
        foreach ($detail_pembayaran->result() as $value) {
          # code...
          if ($value->status_pembayaran=='belum_lunas' && $value->status_pengambilan == 'belum_diambil') {
            # code...
          ?>
          <?php $id_detail_order = $value->id_detail_order; ?>
          <!--<a href="<?php echo base_url(); ?>order/set_status_pembayaran/<?php echo $value->id_detail_order."/".$total_bayaran; ?>"><button type="button" class="btn btn-block btn-primary btn-sm"><i class="fa fa-check"></i> Lunas</button></a>-->
          <button type="button" class="btn btn-block btn-primary btn-sm" id="button_lunas"><i class="fa fa-check"></i>Belum Lunas</button>
          <button type="button" class="btn btn-block btn-danger btn-sm" id="button_status" data-id="<?php echo $id_detail_order; ?>"><i class="fa fa-check"></i> Belum Diambil</button>
          <?php
          }
          elseif($value->status_pembayaran=='lunas' && $value->status_pengambilan == 'belum_diambil'){
          ?>
          <?php $id_detail_order = $value->id_detail_order; ?>
          <button type="button" class="btn btn-block btn-primary disabled" id="button_lunas"><i class="fa fa-check"></i></a> Lunas</button>
         <button type="button" class="btn btn-block btn-danger btn-sm" id="button_status" data-id="<?php echo $id_detail_order; ?>"><i class="fa fa-check"></i> Belum Diambil</button>
          <?php
          }
          elseif($value->status_pembayaran=='belum_lunas' && $value->status_pengambilan == 'sudah_diambil'){
          ?>
          <?php $id_detail_order = $value->id_detail_order; ?>
          <!--<a href="<?php echo base_url(); ?>order/set_status_pembayaran/<?php echo $value->id_detail_order."/".$total_bayaran; ?>"><button type="button" class="btn btn-block btn-primary btn-sm"><i class="fa fa-check"></i> Lunas</button></a>-->
          <button type="button" class="btn btn-block btn-primary btn-sm" id="button_lunas"><i class="fa fa-check"></i>Belum Lunas</button>
          <button type="button"  class="btn btn-block btn-danger disabled" id="button_lunas"><i class="fa fa-check"></i></a> Sudah Diambil</button>
          <?php
          }
          else{
            ?>
          <button type="button" class="btn btn-block btn-primary disabled" id="button_lunas"><i class="fa fa-check"></i></a> Lunas</button>
          <button type="button"  class="btn btn-block btn-danger disabled" id="button_lunas"><i class="fa fa-check"></i></a> Sudah Diambil</button>
          <?php
          }
        }
        ?>
      </td>
    </tr>
  </tbody>
</table>

<script>
  $('#button_lunas').click(function(){
      var total_bayar = <?php echo $total_bayaran; ?>;
      var sisa_bayar = '0,00';
      var value = total_bayar.toLocaleString( 'de-DE', { minimumFractionDigits: 2 } );

      var url = '<?php echo base_url()."order/set_status_pembayaran/".$id_detail_order."/".$total_bayaran; ?>';
      // alert(url);
      $.get(url, function(data, status){
        $("#sisa_bayar").html(sisa_bayar);

        alert('Data berhasil di update');
        $("#button_lunas").addClass("disabled");
      });
  });

  $("#button_status").click(function(){
      var id_detail_order = $(this).attr('data-id');
      $.ajax({
        url: "<?php echo base_url(); ?>order/set_status_pengambilan",
        type: "POST",
        data:{'id_detail_order':id_detail_order},
        success: function(){
          $("#button_status").addClass("disabled").html('<i class="fa fa-check"></i></a> Sudah Diambil');
        }
      })
  });
</script>