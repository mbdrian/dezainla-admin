<?php
foreach ($user->result() as $user_param) {
	# code...
?>
<form method="post" action="<?php echo base_url('user/proses_edit_user')?>" id="form" class="form-horizontal">
	<input type="hidden" name="id_user" value="<?php echo $user_param->id_user; ?>">
	<div class="form-body">
		<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
		    <div class="col-sm-5">
		      <input type="text" class="form-control" placeholder="Nama" name="nama" value="<?php echo $user_param->nama; ?>">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Username</label>
		    <div class="col-sm-5">
		      <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $user_param->username; ?>">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
		    <div class="col-sm-5">
		      <input type="password" class="form-control" placeholder="Password" name="password">
		    </div>
	  	</div>
	  	<div class="form-group">
		    <label for="inputEmail3" class="col-sm-3 control-label">Level</label>
		    <div class="col-sm-3">
		      <select class="form-control" name="level">
		        <option>-- Pilih Level --</option>
		        <option <?php if($user_param->level=='admin')echo 'selected="selected"';?>value="admin">Admin</option>
		        <option <?php if($user_param->level=='produksi')echo 'selected="selected"';?> value="produksi">Produksi</option>
		        <option <?php if($user_param->level=='desainer')echo 'selected="selected"';?>value="desainer">Desainer</option>
		        <option <?php if($user_param->level=='ceo')echo 'selected="selected"';?>value="ceo">CEO</option>
		      </select>
	    </div>
	</div>
	<div class="box-footer">
	    <button type="reset" class="btn btn-default">Reset</button>
	    <button type="submit" class="btn btn-info">Update</button>
	</div>
</form>
<?php
}
?>