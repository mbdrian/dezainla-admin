    <link rel="stylesheet" href="<?= base_url('assets/order/bootstrap-3.3.5/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/order/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/order/datatables/css/dataTables.bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!--<script src="<?= base_url('assets/order/jquery-2.1.4.min.js') ?>"></script>-->
    <script type='text/javascript' src='<?php echo site_url('assets/autocomplete/js/jquery.autocomplete.js');?>'></script>
    <link href='<?php echo site_url('assets/autocomplete/js/jquery.autocomplete.css');?>' rel='stylesheet' />
    <link href='<?php echo site_url('assets/autocomplete/css/default.css');?>' rel='stylesheet' />
    <script src="<?= base_url('assets/order/bootstrap-3.3.5/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/order/datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/order/datatables/js/dataTables.bootstrap.js') ?>"></script>
    <script src="<?= base_url('assets/order/maskMoney/jquery.maskMoney.min.js') ?>"></script>
            <?php
foreach ($order_user->result() as $order_user_item) {
  # code...
?>
<?php echo $this->session->flashdata('status');?>
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>order/proses_edit_data_order">
  <input type="hidden" name="id_detail_order" value="<?php echo $order_user_item->id_detail_order;?>">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Order</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="tanggal_order" value="<?php echo date('d-m-Y',strtotime($order_user_item->tanggal_order));?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Nama Konsumen</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="nama_konsumen" value="<?php echo $order_user_item->nama_konsumen; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">No Handphone</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="hp_konsumen" value="<?php echo $order_user_item->hp_konsumen; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Penerima File</label>
    <div class="col-sm-2">
      <select class="form-control" name="penerima_file">
        <option>-- Pilih Penerima File --</option>
        <?php
        foreach ($penerima_file->result() as $penerima_file_item) {
          # code...
        ?>
        <option <?php if($order_user_item->id_user==$penerima_file_item->id_user){echo "selected";} ?> value="<?php echo $penerima_file_item->id_user; ?>"><?php echo $penerima_file_item->nama ?></option>
        <?php
        }
        ?>
        
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Tanggal Pengambilan</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="tanggal_pengambilan" value="<?php echo date('m/d/Y',strtotime($order_user_item->tanggal_pengambilan));?>" id="datepicker">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Waktu Pengambilan</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" name="waktu_pengambilan" value="<?php echo $order_user_item->waktu_pengambilan;?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Keterangan</label>
    <div class="col-sm-5">
        <?php foreach ($detail_order_produk->result() as $value) { ?>
          <textarea class="form-control" name="keterangan" rows="5" ><?php echo trim($value->keterangan); ?></textarea>
        <?php }?>
    </div> 
  </div>
  <div class="box">
            <div class="box-header">
              <h3 class="box-title">Detail Order</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody>
                <tr>
                  <th>ID Produk</th>
                  <th>Nama Barang</th>
                  <th>Harga</th>
                  <th>Panjang</th>
                  <th>Lebar</th>
                  <th>Kuantitas</th>
                  <th>Subtotal</th>
                  <th>Keterangan</th>
                  <th>Aksi</th>
                </tr>
                <?php
                foreach ($detail_order->result() as $detail_order_item) {
                  # code...
                ?>
                <tr>
                  <td><?php echo $detail_order_item->id_detail_produk; ?></td>
                  <td><?php echo $detail_order_item->jenis_produk; ?></td>
                  <td><?php echo $detail_order_item->harga_produk; ?></td>
                  <td><?php echo $detail_order_item->panjang; ?></td>
                  <td><?php echo $detail_order_item->lebar; ?></td>
                  <td><?php echo $detail_order_item->kuantitas; ?></td>
                  <td><?php echo $detail_order_item->subtotal; ?></td>
                  <td><?php echo $detail_order_item->keterangan; ?></td>
                  <td><a href="<?php echo base_url();?>order/delete_order_produk/<?php echo $detail_order_item->id_detail_order; ?>/<?php echo $detail_order_item->id_order_produk; ?>/<?php echo $detail_order_item->subtotal; ?>" class="btn btn-warning" onClick="return konfirmasi()"><i class="fa fa-trash"></i> Hapus</a></td>
                </tr>
                <?php
                }
                ?>
                <?php
                $total_bayar = 0;
                  foreach ($detail_order_produk->result() as $value) {
                    # code...
                    $total_bayar += $value->total_bayar;
                  ?>
                <tr>
                  <td colspan="7"></td>
                  <td><b>Total Bayar</b></td>
                  <td><b><input type="number" id="total" value="<?php echo $value->total_bayar;?>" readonly="readonly"></b></td>
                  <input type="hidden" name="total_bayar" value="<?php echo $total_bayar;?>">
                </tr>
                <tr>
                  <td colspan="7"></td>
                  <td><b>Uang Muka</b></td>
                  <td><b><input type="number" name="uang_muka" id="bayar" onkeyup="showKembali(this.value)" value="<?php echo $value->uang_muka;?>"></b></td>
                </tr>
                <tr>
                  <td colspan="7"></td>
                  <td><b>Diskon</b></td>
                  <td><b><input type="number" name="diskon" id="diskon" onkeyup="showDiskon(this.value)" value="<?php echo $value->diskon;?>"></b></td>
                </tr>
                <tr>
                  <td colspan="7"></td>
                  <td><b>Sisa Pembayaran</b></td>
                  <td><b><input type="number" name="sisa_pembayaran" id="kembali" value="<?php echo $value->sisa_pembayaran;?>" readonly="readonly"></b></td>

                </tr>
                  <?php
                  }
                  ?>
              </tbody></table>
              <div id="notif" hidden="hidden">
              <span class="label label-danger pull-right">Sisa Pembayaran Tidak Boleh Lebih Dari 0</span>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
  <div class="box-footer">
      <button type="submit" class="btn btn-info">Update</button>
  </div>
</form>
<?php
}
?>

<script>
function showKembali(str)
    {
        var total = $('#total').val().replace(".", "").replace(".", "");
        var bayar = str.replace(".", "").replace(".", "");
        var kembali = bayar-total;

        if (kembali < 0){
          $('#kembali').val(kembali);
          $('#notif').hide();
        }
        else{
           $('#kembali').val(0);
           $('#notif').show();
        }

        
    }

    function showDiskon(str)
    {
        var total = $('#total').val().replace(".", "").replace(".", "");
        var uang_muka = $('#bayar').val().replace(".", "").replace(".", "");
        var bayar = str.replace(".", "").replace(".", "");
        var sisa = total-uang_muka;
        var kembali = bayar-sisa;
        if (kembali < 0){
          $('#kembali').val(kembali);
          $('#notif').hide();
        }
        else{
           $('#kembali').val(0);
           $('#notif').show();
        }
        

        
    }

function konfirmasi(){
    var pilihan=confirm("Apakah Anda yakin ingin menghapus?");
    if (pilihan) {
      return true;
    }
    else{
      return false;
    }
  }
</script>
<script>
$(function () {
  $('#datepicker').datepicker({
      autoclose: true
    });
 });
</script>