<?php
foreach ($master_produk->result_array() as $master_produk_item) {
	# code...
?>
<div class="col-md-6">
	<form role="form" action="<?php echo base_url(); ?>produk/proses_edit_master_produk" method="post">
		<input type="hidden" name="id_produk" value="<?php echo $master_produk_item['id_produk']; ?>">
		<div class="form-group">
			<label>ID  Produk</label>
			<input class="form-control" placeholder="ID Produk" name="id_produk2" value="<?php echo $master_produk_item['id_produk']; ?>">
		</div>
		<div class="form-group">
			<label>Nama Produk</label>
			<input class="form-control" placeholder="Nama Produk" name="nama_produk" value="<?php echo $master_produk_item['nama_produk']; ?>">
		</div>
		<button type="submit" class="btn btn-primary">Edit</button>
	</form>
</div>
<?php
}
?>