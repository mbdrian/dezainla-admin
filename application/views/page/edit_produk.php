<?php
foreach ($produk->result_array() as $produk_item) {
	# code...
?>
<div class="col-md-6">
	<form role="form" action="<?php echo base_url(); ?>produk/proses_edit_produk" method="post">
		<input type="hidden" name="id_detail_produk" value="<?php echo $produk_item['id_detail_produk']; ?>">
		<div class="form-group">
			<label>ID  Produk</label>
			<input class="form-control" placeholder="ID Produk" name="id_detail_produk_baru" value="<?php echo $produk_item['id_detail_produk']; ?>">
		</div>
		<div class="form-group">
			<label>Jenis  Produk</label>
			<input class="form-control" placeholder="Jenis Produk" name="jenis_produk" value="<?php echo $produk_item['jenis_produk']; ?>">
		</div>
		<div class="form-group">
			<label>Harga Produk</label>
			<input class="form-control" placeholder="Harga Produk" name="harga_produk" value="<?php echo $produk_item['harga_produk']; ?>">
		</div>
		<?php if($this->session->userdata('level')=='ceo'){ ?>
		<div class="form-group">
			<label>HPP</label>
			<input class="form-control" placeholder="HPP" name="hpp" value="<?php echo $produk_item['hpp']; ?>">
		</div>
		<?php } ?>
		<div class="form-group">
			<label>Keterangan</label>
			<input class="form-control" placeholder="Keterangan" name="keterangan" value="<?php echo $produk_item['keterangan']; ?>">
		</div>
		<div class="form-group">
			<label>Kategori Produk</label>
			<select class="form-control" name="id_produk">
				<option>-- Pilih Kategori --</option>
				<?php
				foreach ($kategori_produk->result_array() as $kategori_produk_item) {
					# code...
				?>
				<option <?php if($produk_item['id_produk']==$kategori_produk_item['id_produk'])echo 'selected="selected"'; ?> value="<?php echo $kategori_produk_item['id_produk']; ?>"><?php echo $kategori_produk_item['nama_produk']; ?></option>
				<?php
				}
				?>
			</select>
		</div>
		<button type="submit" class="btn btn-primary">Edit</button>
	</form>
</div>
<?php
}
?>
<script type="text/javascript">
	function btnClick() {
    	window.header("produk/daftar_produk");
    }
</script>
