<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<div class="row">
	<div class="col-md-12">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Informasi Detail Order</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table class="table table-striped">
                <tbody><tr>
                  <th style="width: 10px">No</th>
                  <th>ID Produk</th>
                  <th>Nama Produk</th>
                  <th>Harga</th>
                  <th style="width: 20px">Panjang</th>
                  <th style="width: 20px">Lebar</th>
                  <th style="width: 20px">Kuantitas</th>
                  <th>Subtotal</th>
                 <?php
					$no=1;
          $id_detail_order="";
          $tanggal_pengambilan="";
					foreach ($tracking_order->result_array() as $tracking_order_item){ 
						# code...
            $id_detail_order = $tracking_order_item['id_detail_order'];
            $tanggal_pengambilan = $tracking_order_item['tanggal_pengambilan'];
				 ?>
                </tr>
                <tr>
                  <td><?php echo $no++; ?></td>
                  <td><?php echo $tracking_order_item['id_detail_produk']; ?></td>
                  <td><?php echo $tracking_order_item['jenis_produk']; ?></td>
                  <td><?php echo $tracking_order_item['harga_produk']; ?></td>
                  <td><?php echo $tracking_order_item['panjang']; ?></td>
                  <td><?php echo $tracking_order_item['lebar']; ?></td>
                  <td><?php echo $tracking_order_item['kuantitas']; ?></td>
                  <td><?php echo $tracking_order_item['subtotal']; ?></td>
                 <?php
				  }
				 ?>
                </tr>
              </tbody>
             </table>
            </div>
            <!-- /.box-body -->
        </div>
	</div>
</div>


<div class="row">
	<div class="col-md-4">
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Ubah Tanggal Deadline</h3>
            </div>
            <!-- /.box-header -->
           	<form role="form" action="<?php echo base_url(); ?>order/proses_tanggal_deadline_baru" method="post">
           		<div class="form-group">
					<label>Tanggal Deadline</label>
					
					<input type="hidden" value="<?php echo $id_detail_order; ?>" name="id_detail_order">
					<input class="form-control" value="<?php echo date('d-m-Y',strtotime($tanggal_pengambilan)); ?>" disabled="disabled">
				</div>
				<div class="form-group">
                      <label>Tanggal Deadline Baru</label> 
                        <input type="text" class="form-control" 
                            name="tanggal_deadline_baru" id="datepicker" data-date-format='dd-mm-yyyy' placeholder="dd-mm-yyyy">
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
           	</form>
        </div>
	</div>
</div>
<script>
$(function () {
  $('#datepicker').datepicker({
      autoclose: true
    });
 });
</script>