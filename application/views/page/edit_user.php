<?php
foreach ($user->result_array() as $user_item) {
	# code...
?>
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>user/proses_edit_user">
  <input type="hidden" name="id_user" value="<?php echo $user_item['id_user']; ?>">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Username</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo $user_item['username']; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Password" name="password">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="nama" name="nama" value="<?php echo $user_item['nama']; ?>">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Level</label>
    <div class="col-sm-2">
      <select class="form-control" name="level">
        <option>-- Pilih Level --</option>
        <option <?php if($user_item['level']=='admin')echo 'selected="selected"';?>value="admin">Admin</option>
        <option <?php if($user_item['level']=='operator')echo 'selected="selected"';?> value="operator">operator</option>
      </select>
    </div>
  </div>
  <div class="box-footer">
      <button type="reset" class="btn btn-default">Reset</button>
      <button type="submit" class="btn btn-info">Edit</button>
  </div>
</form>
<?php
}
?>