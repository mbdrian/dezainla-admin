    <link rel="stylesheet" href="<?= base_url('assets/order/bootstrap-3.3.5/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/order/font-awesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/order/datatables/css/dataTables.bootstrap.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
    <script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!--<script src="<?= base_url('assets/order/jquery-2.1.4.min.js') ?>"></script>-->
    <script type='text/javascript' src='<?php echo site_url('assets/autocomplete/js/jquery.autocomplete.js');?>'></script>
    <link href='<?php echo site_url('assets/autocomplete/js/jquery.autocomplete.css');?>' rel='stylesheet' />
    <link href='<?php echo site_url('assets/autocomplete/css/default.css');?>' rel='stylesheet' />
    <script src="<?= base_url('assets/order/bootstrap-3.3.5/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/order/datatables/js/jquery.dataTables.min.js') ?>"></script>
    <script src="<?= base_url('assets/order/datatables/js/dataTables.bootstrap.js') ?>"></script>
    <script src="<?= base_url('assets/order/maskMoney/jquery.maskMoney.min.js') ?>"></script>
    <script type='text/javascript'>
        var site = "<?php echo site_url();?>";
        $(function(){
            $('#autocomplete1').change(function(){
                $('#hp_konsumen').prop('disabled', false);
                $('#hp_konsumen').val('');
                $('#hp_konsumen').attr('placeholder','Isi nomor handphone');
            });
            $('.autocomplete').autocomplete({
                // serviceUrl berisi URL ke controller/fungsi yang menangani request kita
                serviceUrl: site+'dashboard/search',
                // fungsi ini akan dijalankan ketika user memilih salah satu hasil request
                onSelect: function (suggestion) {
                    $('#hp_konsumen').val(''+suggestion.hp_konsumen); // membuat id 'hp_konsumen' untuk ditampilkan
                    $('#hp_konsumen').attr('disabled','disabled');
                    $('#hp_konsumen2').val($('#hp_konsumen').val());
                }
            });
            
            $("#selesai").click(function() {
                // alert("test button submit");
                var nama_konsumen = $("#autocomplete1").val();
                // alert(nama_konsumen);

                var isHpKonsumenDisabled = $('#hp_konsumen').is(':disabled')
                if(isHpKonsumenDisabled) {
                    $("#form_transaksi").submit();                    
                }
                else {
                    $.ajax({ type: "GET",   
                            url: site+'dashboard/searchByNameConsumer/'+nama_konsumen,
                            async: false,
                            success : function(data)
                            {
                                response = data;
                                // alert(response);
                                if(response>=1){
                                    alert("Error: Nama Konsumen "+nama_konsumen+" sudah ada di sistem. Harap memilih pilihan yang diberikan oleh sistem");
                                } else {
                                    $("#form_transaksi").submit();
                                }
                            }
                    });
                }

            });
        });
    </script>
    <style type="text/css">
        #header,#footer{
            background-color: #337ab7;
            color: #fff;
            text-align: center;
        }
        #header{
            margin-bottom: 30px;
        }
        #header h1{
            margin: 0;
            padding: 15px;
        }
        #footer{
            padding: 3px;
        }
        .btn{
            border-radius: 2px;
        }
        .btn-kecil{
            padding: 0 6px 0 6px;
        }
        .form-control[disabled], .form-control[readonly], 
        fieldset[disabled] .form-control{
            background-color: #EBF2F8;
        }
        .besar{
            font-size: 20px;
            font-weight: 300;
        }
        table th,table td{
            text-align: center;
        }
        form{
            margin-top: 20px;
        }
        .mb{
            margin-bottom: 30px;
        }
        .nav ul li{
            list-style: none;
        }
        .nav ul{
            padding-left: 20px;
            
        }
        .nav ul li a{
            text-decoration: none;
            display: block;
            padding: 4px;
            margin: 3px;
        }
        .nav ul li a:hover{
            text-decoration: none;
            color: #fff;
            background-color: #337ab7;
            border-radius: 2px;
        }
        .nav>li>a:hover{
            background-color: #337ab7;
            color: #fff;
        }
        .nav ul .active{
            background-color: #337ab7;
            border-radius: 2px;
        }
        .nav ul .active a{
            color: #fff;
        }
        .nav li a:active,.nav li a:focus{
            background-color: #337ab7;
            border-radius: 2px;
            color: #fff;
        }
    </style>
<div class="row">
  <div class="col-md-12">
    <div class="col-md-12">
        <div class="panel panel-default">
         <div class="panel-body">
            <?php echo $this->session->flashdata('status'); ?>
            <form class="form-horizontal" id="form_transaksi" role="form" method="post" action="<?php echo base_url(); ?>order/proses_order">
            <div class="col-md-8">
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                      >Tanggal :</label>
                      <div class="col-md-4">
                        <input type="text" class="form-control" 
                            name="tanggal"  
                            value="<?php echo date('Y-m-d'); ?>">
                      </div>
                    </div>
                    <div id="content">
                      <div class="form-group">
                      <label class="control-label col-md-3" 
                      >Nama Konsumen :</label>
                      <div class="col-md-8">
                        <input type="text" class="autocomplete nama form-control" style="font-size:12px" 
                            name="nama_konsumen" id="autocomplete1" placeholder='Isi nama pelanggan'
                        >
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                      >No Handphone :</label>
                      <div class="col-md-4">
                        <input type="hidden" class="autocomplete form-control" 
                                name="hp_konsumen" id="hp_konsumen2" placeholder='Isi nomor handphone'
                            >
                        <input type="text" class="autocomplete form-control" 
                            name="hp_konsumen" id="hp_konsumen" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder='Isi nomor handphone'
                        >
                      </div>
                      <label class="col-md-5"> *Nomor Handphone hanya diisi angka </label>
                    </div>
                    </div>
                    
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">Penerima File</label>
                      <div class="col-sm-4">
                        <select class="form-control" name="nama_penerima_file">
                          <option value="">-- Pilih Penerima --</option>
                          <?php
                          foreach ($penerima_file->result_array() as $penerima_file_item) {
                            # code...
                          ?>
                          <option value="<?php echo $penerima_file_item['id_user']; ?>"><?php echo $penerima_file_item['nama']; ?></option>
                          <?php
                          }
                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                      >Tanggal Pengambilan :</label>
                      <div class="col-md-4">
                        <input type="text" class="form-control" 
                            autocomplete="off"
                            name="tanggal_pengambilan" id="datepicker" data-date-format='dd-mm-yyyy' placeholder="dd-mm-yyyy">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" >Waktu Pengambilan :</label>
                      <div class="col-md-3">
                        <input type="time" class="form-control" name="waktu_pengambilan" >
                      </div>
                    </div>
                   
            </div>
            <div class="col-md-8">
                <div class="form-group">
                  <label class="control-label col-md-3" 
                    for="id_barang">Id Barang :</label>
                  <div class="col-md-5">
                    <input list="list_barang" class="form-control reset" 
                        placeholder="Isi id..." name="id_barang" id="id_barang" 
                        autocomplete="off" onchange="showBarang(this.value)">
                      <datalist id="list_barang">
                        <?php foreach ($barang->result_array() as $barang): ?>
                            <option value="<?= $barang['id_detail_produk']; ?>"><?= $barang['jenis_produk']; ?></option>
                        <?php endforeach ?>
                      </datalist>
                  </div>
                </div>
                <div id="barang">
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                        for="nama_barang">Nama Barang :</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control reset" 
                            name="nama_barang" id="nama_barang" 
                            readonly="readonly">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                        for="harga_barang">Harga (Rp) :</label>
                      <div class="col-md-8">
                        <input type="text" class="form-control reset" 
                            name="harga_barang" id="harga_barang" 
                            readonly="readonly">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                        for="qty">Panjang :</label>
                      <div class="col-md-4">
                        <input type="number" class="form-control reset" 
                            autocomplete="off" onchange="subTotal(this.value)" 
                            onkeyup="subTotal(this.value)" id="panjang" min="0" 
                            name="panjang" placeholder="Isi Panjang...">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                        for="qty">Lebar :</label>
                      <div class="col-md-4">
                        <input type="number" class="form-control reset" 
                            autocomplete="off" onchange="subTotal(this.value)" 
                            onkeyup="subTotal(this.value)" id="lebar" min="0" 
                            name="lebar" placeholder="Isi Lebar...">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="control-label col-md-3" 
                        for="qty">Quantity :</label>
                      <div class="col-md-4">
                        <input type="number" class="form-control reset" 
                            autocomplete="off" onchange="subTotal(this.value)" 
                            onkeyup="subTotal(this.value)" id="qty" min="0" 
                            name="qty" placeholder="Isi qty...">
                      </div>
                    </div>
                </div><!-- end id barang -->
                <div class="form-group">
                  <label class="control-label col-md-3" 
                    for="sub_total">Sub-Total (Rp):</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control reset" 
                        name="sub_total" id="sub_total" 
                        readonly="readonly">
                  </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 col-md-3">
                        <button type="button" class="btn btn-primary" 
                        id="tambah" onclick="addbarang()">
                          <i class="fa fa-cart-plus"></i> Tambah</button>
                    </div>
                </div>
                  <!-- </div>
                </div> --><!-- end panel-->
            </div><!-- end col-md-8 -->
            <div class="col-md-4 mb">
                <div class="col-md-12">
                    <div class="form-group">
                      <label for="total" class="besar">Total (Rp) :</label>
                        <input type="number" class="form-control input-lg" 
                        name="total" id="total" placeholder="0"
                        readonly="readonly"  value="<?= number_format( 
                        $this->cart->total(), 0 , '' , '.' ); ?>">
                    </div>
                    <div class="form-group">
                      <label for="bayar" class="besar">Uang Muka (Rp) :</label>
                        <input type="number" class="form-control input-lg uang" 
                            name="bayar" placeholder="0" autocomplete="off"
                            id="bayar" onkeyup="showKembali(this.value)" value=0>
                    </div>
                    <div class="form-group">
                      <label for="diskon" class="besar">Diskon (Rp) :</label>
                        <input type="number" class="form-control input-lg uang" 
                            name="diskon" placeholder="0" autocomplete="off"
                            id="diskon" onkeyup="showDiskon(this.value)" value=0>
                    </div>
                    <div class="form-group">
                      <label for="kembali" class="besar">Sisa Pembayaran (Rp) :</label>
                        <input type="number" class="form-control input-lg" 
                        name="kembali" id="kembali" placeholder="0"
                        readonly="readonly">
                        <br>
                        <div id="notif" hidden="hidden">
                          <span class="label label-danger">Sisa Pembayaran Tidak Boleh Lebih Dari 0</span>
                        </div>
                    </div>
                </div>
            </div><!-- end col-md-4 -->
            <table id="table_transaksi" class="table table-striped 
                table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Id Barang</th>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Quantity</th>
                        <th>Sub-Total</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="col-md-8">
              <div class="form-group">
                      <label class="control-label col-md-3" 
                      >Keterangan :</label>
                      <div class="col-md-8">
                        <textarea class="form-control" name="keterangan" rows="3" placeholder="Ketik disini ..."></textarea>
                      </div>
                    </div>
            </div>
            
          </div>
        </div>
    </div>
    </form>
    <div class="col-md-offset-8" style="margin-top:120px">
                <button type="submit" class="btn btn-primary btn-lg" 
                id="selesai"
                >
                Selesai <i class="fa fa-angle-double-right"></i></button>
            </div>
  </div>
</div>
<script type="text/javascript">
    function redirect(){
      location.reload();
    }

    function showBarang(str) 
    {

        if (str == "") {
            $('#nama_barang').val('');
            $('#harga_barang').val('');
            $('#qty').val('');
            $('#sub_total').val('');
            $('#reset').hide();
            return;
        } else { 
          if (window.XMLHttpRequest) {
              // code for IE7+, Firefox, Chrome, Opera, Safari
               xmlhttp = new XMLHttpRequest();
          } else {
              // code for IE6, IE5
              xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
          }
          xmlhttp.onreadystatechange = function() {
               if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                  document.getElementById("barang").innerHTML = 
                  xmlhttp.responseText;
              }
          }
          xmlhttp.open("GET", "<?= site_url('order/getbarang'); ?>/"+str,true);
          xmlhttp.send();
        }
    }

    function subTotal(qty)
    {

        var harga = $('#harga_barang').val().replace(".", "").replace(".", "");
        var panjang = $('#panjang').val();
        var lebar = $('#lebar').val();

        $('#sub_total').val(convertToRupiah(panjang*lebar*harga*qty));
    }

    function convertToRupiah(angka)
    {

        var rupiah = '';    
        var angkarev = angka.toString().split('').reverse().join('');
        
        for(var i = 0; i < angkarev.length; i++) 
          if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        
        return rupiah.split('',rupiah.length-1).reverse().join('');
    
    }

    var table;
    $(document).ready(function() {

      showKembali($('#bayar').val());

      table = $('#table_transaksi').DataTable({ 
        paging: false,
        "info": false,
        "searching": false,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' 
        // server-side processing mode.
        
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?= site_url('order/ajax_list_transaksi')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
          "targets": [ 0,1,2,3,4,5,6 ], //last column
          "orderable": false, //set not orderable
        },
        ],

      });
    });

    function reload_table()
    {

      table.ajax.reload(null,false); //reload datatable ajax 
    
    }

    function addbarang()
    {
        var id_barang = $('#id_barang').val();
        var qty = $('#qty').val();
        if (id_barang == '') {
          $('#id_barang').focus();
        }else if(qty == ''){
          $('#qty').focus();
        }else{
       // ajax adding data to database
          $.ajax({
            url : "<?= site_url('order/addbarang')?>",
            type: "POST",
            data: $('#form_transaksi').serialize(),
            dataType: "JSON",
            success: function(data)
            {
               //reload ajax table
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding data');
            }
        });

          showTotal();
          showKembali($('#bayar').val());
          //mereset semua value setelah btn tambah ditekan
          $('.reset').val('');
        };
    }

    function deletebarang(id,sub_total)
    {
        // ajax delete data to database
          $.ajax({
            url : "<?= site_url('order/deletebarang')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

          var ttl = $('#total').val().replace(".", "");

          $('#total').val(convertToRupiah(ttl-sub_total));

          showKembali($('#bayar').val());
    }

    function showTotal()
    {

        var total = $('#total').val().replace(".", "").replace(".", "");

        var sub_total = $('#sub_total').val().replace(".", "").replace(".", "");

        $('#total').val((Number(total)+Number(sub_total)));

    }

    //maskMoney
    $('.uang').maskMoney({
        thousands:'.', 
        decimal:',', 
        precision:0
    });

    function showKembali(str)
    {
        var total = $('#total').val().replace(".", "").replace(".", "");
        var bayar = str.replace(".", "").replace(".", "");
        var kembali = bayar-total;

        $('#kembali').val(kembali);

        
    }

    function showDiskon(str)
    {
        var total = $('#total').val().replace(".", "").replace(".", "");
        var uang_muka = $('#bayar').val().replace(".", "").replace(".", "");
        var bayar = str.replace(".", "").replace(".", "");
        var sisa = total-uang_muka;
        var kembali = bayar-sisa;
        if (kembali < 0){
          $('#kembali').val(kembali);
          $('#notif').hide();
        }
        else{
           $('#kembali').val(0);
           $('#notif').show();
        }
        

        
    }
   
</script>
<script>
$(function () {
  $('#datepicker').datepicker({
      autoclose: true
    });
 });
</script>