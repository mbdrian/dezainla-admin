<div class="row">
  <div class="col-xs-12">
    <div class="box box-info">
    <div class="box-header with-border">
    </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form action="<?php echo base_url(); ?>user/update_password_user" method="post">
<?php echo $this->session->flashdata('status');?>

              <div class="box-body">
                <input type="hidden" name="id" value="<?php echo $id_user; ?>">

                <div class="form-group">
                  <label for="exampleInputEmail1">Password Baru</label>
                  <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Password" style="width:30%" name="password" value="<?php echo set_value('password'); ?>">
                </div>

                <div class="form-group">
                  <label for="exampleInputEmail1">Konfirmasi Password Baru</label>
                  <input type="password" class="form-control" id="exampleInputEmail1" placeholder="Password" style="width:30%" name="temp_password" value="<?php echo set_value('temp_password'); ?>">
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info">Ganti Password</button>
              </div>
              <!-- /.box-footer -->
            </form>
    </div>
  </div>
</div>