<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
<form class="form-horizontal" action="<?php echo base_url(); ?>order/proses_rekapitulasi_order_bulanan" method="post">
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-3 control-label">Dari</label>
      <div class="col-sm-6">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker"  name="tanggal_mulai" autocomplete="off">
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
      <div class="col-sm-6">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker2" name="tanggal_selesai" autocomplete="off">
        </div>
      </div>
    </div>
  </div>
</div>
<div>
    <button type="reset" class="btn btn-default">Reset</button>
    <button type="submit" class="btn btn-info">Submit</button>
</div>
</form>
<div class="box-footer">

</div>
<?php
if (isset($rekapitulasi_order)) {
  # code...
?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped" id="example1">
      <thead>
        <tr>      
          <th>No</th>
          <th>Tanggal Order</th>
          <th>ID Order</th>
          <th>Jenis Produk</th>
          <th>Nama Konsumen</th>
          <th>No Handphone</th>
          <th>Penerima File</th>
          <th>Subtotal</th>
          <th>Diskon</th>
          <th>Sisa Pembayaran</th>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<th>HPP</th>';
          } ?>
        </tr>
      </thead>
      <tbody>
        <?php
        $no=1;
        $total_harga=0;
        $sisa_pembayaran=0;
        // $uang_muka=0;
        $diskon=0;
        $hpp = 0;
        foreach ($rekapitulasi_order->result_array() as $rekapitulasi_order_item) {
          if($rekapitulasi_order_item['sisa_pembayaran']<0){
            $rekapitulasi_order_item['sisa_pembayaran'] *= -1;
          }
        ?>
        <tr>
          <input type="hidden" name="tanggal">    
          <td><?php echo $no++;?></td>
          <td><?php echo date('d-m-Y',strtotime($rekapitulasi_order_item['tanggal_order'])); ?></td>    
          <td><?php echo $rekapitulasi_order_item['id_detail_order']; ?></td>
          <td><?php echo $rekapitulasi_order_item['jenis_produk']; ?></td>
          <td><?php echo $rekapitulasi_order_item['nama_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_order_item['hp_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_order_item['nama']; ?></td>
          <td>Rp. <?php echo number_format($rekapitulasi_order_item['subtotal'], "2", ",", "."); ?></td>
          <td><?php echo "Rp. ".number_format($rekapitulasi_order_item['diskon'], "2", ",", ".");  ?></td>
          <td><?php echo "Rp. ". number_format($rekapitulasi_order_item['sisa_pembayaran'], "2", ",", "."); ?></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td>Rp. '.number_format($rekapitulasi_order_item['hpp'], "2", ",", ".").'</td>';
          } ?>        
        </tr>
        <?php
        $total_harga += $rekapitulasi_order_item['subtotal'];
        $sisa_pembayaran += $rekapitulasi_order_item['sisa_pembayaran'];
        // $uang_muka += $rekapitulasi_order_item['uang_muka'];
        $diskon += $rekapitulasi_order_item['diskon'];
        $hpp += $rekapitulasi_order_item['hpp'];
        }
        ?>
        <tr>
          <td colspan="7" align="center"><b>TOTAL</b></td>
          <td><b>Rp. <?php echo number_format($total_harga, "2", ",", "."); ?></b></td>
          <!-- <td><b>Rp. <?php //echo number_format($uang_muka, "2", ",", "."); ?></b></td> -->
          <td><b>Rp. <?php echo number_format($diskon, "2", ",", "."); ?></b></td>
          <td><b>Rp. <?php echo number_format($sisa_pembayaran, "2", ",", "."); ?></b></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td><b>Rp. '.number_format($hpp, "2", ",", ".").'</b></td>';
          } ?>   
        </tr>
     </tbody>
    </table>
      <div class="row no-print">
        
        
        <div class="col-xs-12">
          <form action="<?php echo base_url(); ?>cetak/excel_per_bulan" method="post">
          <input type="hidden" name="tanggal_mulai" value="<?php echo $tanggal_mulai; ?>">
          <input type="hidden" name="tanggal_selesai" value="<?php echo $tanggal_selesai; ?>">
          <button typ="submit" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-file-excel-o"></i> Generate Excel
          </button>
          <a href="<?php echo base_url(); ?>cetak/print_rekapitulasi_bulanan/<?php echo $tanggal_mulai;?>/<?php echo $tanggal_selesai; ?>" target="_blank">
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa fa-print"></i> Print
          </button>
          </a>
          </form>
          
        </div>
      </div>
  </div>
</div>
<?php
}
?>

<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<script>
  $('#datepicker').datepicker({
      autoclose: true
    });
  $('#datepicker2').datepicker({
      autoclose: true
    });
</script>