<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
<div class="modal fade bs-example-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Status Order</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
  </div>
<form class="form-horizontal" action="<?php echo base_url(); ?>order/proses_rekapitulasi_order_by_user" method="post">
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-3 control-label">Dari</label>
      <div class="col-sm-6">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker"  name="tanggal_mulai">
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
      <div class="col-sm-6">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker2" name="tanggal_selesai">
        </div>
      </div>
    </div>
  </div>
</div>
<div>
    <button type="reset" class="btn btn-default">Reset</button>
    <button type="submit" class="btn btn-info">Submit</button>
</div>
</form>
<div class="box-footer">

</div>
<?php
if (isset($rekapitulasi_order)) {
  # code...
?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>      
          <th>No</th>
          <th>ID Order</th>
          <th>Jenis Produk</th>
          <th>Tanggal Order</th>
          <th>Tanggal Pengambilan</th>
          <th>Status</th>
          <th>Deadline</th>
          <th>Detail</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no=1;
        foreach ($rekapitulasi_order->result_array() as $rekapitulasi_order_item) {
          # code...
        ?>
        <tr>    
          <td><?php echo $no++;?></td>    
          <td><?php echo $rekapitulasi_order_item['id_detail_order']; ?></td>
          <td><?php echo $rekapitulasi_order_item['jenis_produk']; ?></td>
          <td><?php echo date('d-m-Y',strtotime($rekapitulasi_order_item['tanggal_order'])); ?></td>
          <td><?php echo date('d-m-Y',strtotime($rekapitulasi_order_item['tanggal_pengambilan'])); ?></td>
          <td><?php echo $rekapitulasi_order_item['status']; ?></td>
          <td>
            <?php
            $date2=date_create(date('Y-m-d'));
            $date1=date_create($rekapitulasi_order_item['tanggal_pengambilan']);
            $diff=date_diff($date1,$date2);
            echo $diff->format("%R%a hari");
            ?>
          </td>
          <td><a href="#" class="edit-record" data-id="<?php echo $rekapitulasi_order_item['id_order_produk']; ?>"><i class="fa fa-search"></i> Detail</a></td>
        </tr>
        <?php
        }
        ?>
     </tbody>
    </table>
  </div>
</div>
<?php
}
?>

<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<script>
  $('#datepicker').datepicker({
      autoclose: true
    });
  $('#datepicker2').datepicker({
      autoclose: true
    });
  $(function () {
    $("#example1").DataTable();
  });

  $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#myModal").modal('show');
                $.post('<?php echo base_url(); ?>order/ubah_status_pengerjaan',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
            });
        });
</script>