<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datepicker/datepicker3.css">
<form class="form-horizontal" action="<?php echo base_url(); ?>user/proses_rekapitulasi_order_user" method="post">
<?php echo $this->session->flashdata('status');?>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Pegawai</label>
    <div class="col-sm-6">
      <select class="form-control" name="nama_pegawai">
        <option>-- Pilih Pegawai --</option>
        <?php
        foreach ($user->result_array() as $user_item) {
          # code...
        ?>
        <option value="<?php echo $user_item['id_user']; ?>"><?php echo $user_item['nama']; ?></option>
        <?php
        }
        ?>  
      </select>
    </div>
  </div>
  </div>
</div>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-3 control-label">Dari</label>
      <div class="col-sm-6">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker"  name="tanggal_mulai">
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-3 control-label">Sampai</label>
      <div class="col-sm-6">
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker2" name="tanggal_selesai">
        </div>
      </div>
    </div>
  </div>
</div>
<div>
    <button type="reset" class="btn btn-default">Reset</button>
    <button type="submit" class="btn btn-info">Submit</button>
</div>
</form>
<div class="box-footer">

</div>
<?php
if (isset($id_user) || isset($tanggal_mulai) || isset($tanggal_selesai)) {
  # code...
?>
<div class="row">
  <div class="col-md-12">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>      
          <th>No</th>
          <th>ID Order Produk</th>
          <th>ID Produk</th>
          <th>Nama Produk</th>
          <th>Nama Pegawai</th>
          <th>Status</th>
      </thead>
      <tbody>
        <?php
        $no=1;
        foreach ($user_pilih->result_array() as $rekapitulasi_order_item) {
          # code...
        ?>
        <tr>  
          <td><?php echo $no++;?></td>
          <td><?php echo $rekapitulasi_order_item['id_detail_order']; ?></td>
          <td><?php echo $rekapitulasi_order_item['id_detail_produk']; ?></td>
          <td><?php echo $rekapitulasi_order_item['jenis_produk']; ?></td>
          <td><?php echo $rekapitulasi_order_item['nama']; ?></td>
          <td><?php echo $rekapitulasi_order_item['status']; ?></td>
        </tr>
        <?php
        }
        ?>
     </tbody>
    </table>
  </div>
</div>
<?php
}
?>

<script src="<?php echo base_url(); ?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<script>
  $('#datepicker').datepicker({
      autoclose: true
    });
  $('#datepicker2').datepicker({
      autoclose: true
    });
  $(function () {
    $("#example1").DataTable();
  });
</script>