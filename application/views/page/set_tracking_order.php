<?php
foreach ($tracking_order->result_array() as $tracking_order_item) {
  # code...
?>
<div class="row">
  <div class="col-md-6">
    <form role="form" method="post" action="<?php echo base_url(); ?>order/proses_set_tracking_order">
      <div class="form-group">
        <label>Nama Produk</label>
        <input type="hidden" value="<?php echo $tracking_order_item['id_order_produk']; ?>" name="id_order_produk">
        <input type="text" class="form-control" readonly="readonly" value="<?php echo $tracking_order_item['jenis_produk']; ?>">
      </div>
      <div class="form-group">
        <label>Quantity</label>
        <input type="text" class="form-control" readonly="readonly" value="<?php echo $tracking_order_item['kuantitas']; ?>">
      </div>
      <div class="form-group">
        <label>Tanggal Pengambilan</label>
        <input type="text" class="form-control" readonly="readonly" value="<?php echo $tracking_order_item['tanggal_pengambilan']; ?>">
      </div>
      <div class="form-group">
        <label>Waktu Pengambilan</label>
        <input type="text" class="form-control" readonly="readonly" value="<?php echo $tracking_order_item['waktu_pengambilan']; ?>">
      </div>
      <div class="form-group">
          <label>Status Order</label>
        
          <select class="form-control" name="status">
          <option> --Pilih Status-- </option>
          <option <?php if($tracking_order_item['status']=='selesai'){echo "selected='selected'";} ?> value="selesai"> Selesai </option>
          <option <?php if($tracking_order_item['status']=='dalam proses'){echo "selected='selected'";} ?> value="dalam proses"> Dalam Proses </option>
          <option <?php if($tracking_order_item['status']=='format salah'){echo "selected='selected'";} ?> value="format salah"> Format Salah </option>
          </select>
      </div>
      <div class="box-footer">
        <button type="reset" class="btn btn-default">Reset</button>
        <button type="submit" class="btn btn-info">Ubah</button>
      </div>
    </form>
  </div>
</div>
<?php
}
?>