<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>produk/proses_tambah_master_produk">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">ID Produk</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="ID Produk" name="id_produk">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Nama Produk</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Nama Produk" name="nama_produk">
    </div>
  </div>
  <div class="box-footer">
      <button type="reset" class="btn btn-default">Reset</button>
      <button type="submit" class="btn btn-info">Tambah</button>
  </div>
</form>