<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>produk/proses_tambah_produk">
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">ID Produk</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="ID Produk" name="id_detail_produk">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Jenis Produk</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Jenis Produk" name="jenis_produk">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Harga Produk</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Harga Produk" name="harga_produk">
    </div>
  </div>
  <?php if($this->session->userdata('level')=='ceo'){ ?>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">HPP</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="HPP" name="hpp">
    </div>
  </div>
  <?php } ?>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Keterangan</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Keterangan" name="keterangan">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Produk</label>
    <div class="col-sm-2">
      <select class="form-control" name="id_produk">
        <option value="">-- Pilih Kategori --</option>
        <?php
        foreach ($produk->result_array() as $produk_item) {
          # code...
        ?>
        <option value="<?php echo $produk_item['id_produk']; ?>"><?php echo $produk_item['nama_produk']; ?></option>
        <?php
        }
        ?>
      </select>
    </div>
  </div>
  <div class="box-footer">
      <button type="reset" class="btn btn-default">Reset</button>
      <button type="submit" class="btn btn-info">Tambah</button>
  </div>
</form>