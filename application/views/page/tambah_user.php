<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>user/proses_tambah_user">
<?php echo $this->session->flashdata('status');?>
    <a href="<?php echo base_url(); ?>user" ><i class="fa fa-angle-left"></i> Kembali</a></li>

    <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Username</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Username" name="username">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Password</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="Password" name="password">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Nama</label>
    <div class="col-sm-5">
      <input type="text" class="form-control" placeholder="nama" name="nama">
    </div>
  </div>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-3 control-label">Level</label>
    <div class="col-sm-2">
      <select class="form-control" name="level">
        <option>-- Pilih Level --</option>
        <option value="admin">Admin</option>
        <option value="produksi">Produksi</option>
      </select>
    </div>
  </div>
  <div class="box-footer">
      <button type="reset" class="btn btn-default">Reset</button>
      <button type="submit" class="btn btn-info">Tambah</button>
  </div>
</form>