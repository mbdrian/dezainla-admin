 <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
<div class="box">
  <!-- /.box-header -->
  <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Order</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
  </div>
  <div class="box-body">
    <table id="mytable" class="table table-bordered table-striped">
      <thead>
        <tr>      
          <th>No</th>
          <th>ID Order</th>
          <th>Nama Konsumen</th>
          <th>No Handphone</th>
          <th>Tanggal Order</th>
          <th>Tanggal Pengambilan</th>
          <th>Designer</th>
          <th>Deadline</th>
          <th>Detail</th>
        </tr>
      </thead>
      
    </table>
  </div>
  <!-- /.box-body -->
</div>

<script src="<?= base_url('assets/order/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<script type="text/javascript">
  $(document).ready(function() {
      $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };

      var t = $("#mytable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                  }
              });
          },
          oLanguage: {
              sProcessing: "loading..."
          },
          processing: true,
          serverSide: true,
          ajax: {"url": "<?php echo base_url();?>order/json_tracking_deadline", "type": "POST"},
          columns: [
              {
                  "data": "id_detail_order",
                  "orderable": false
              },
              {"data": "id_detail_order"},
              {"data": "nama_konsumen"},
              {"data": "hp_konsumen"},
              {"data": "tanggal_order"},
              {"data": "tanggal_pengambilan"},
              {"data": "nama"},
              {
                  "data": function (data){
                      if(data['selisih']=='0 hari'){
                          return 'Hari ini';
                      }
                      return data['selisih'];
                  }
                },
              {
                "data": "action",
                "orderable": false
              }

          ],
          order: [[1, 'asc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              var index = page * length + (iDisplayIndex + 1);
              $('td:eq(0)', row).html(index);
          }
      });
  });
</script>
<script type="text/javascript">
  $(function(){
      $(document).on('click','.edit-record',function(e){
          e.preventDefault();
          $("#myModal").modal('show');
          $.post('<?php echo base_url(); ?>order/tracking_detail_by_id',
              {id:$(this).attr('data-id')},
              function(html){
                  $(".modal-body").html(html);
              }   
          );
      });
  });
</script>    
