 <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
 <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
<div class="box">
  <!-- /.box-header -->
  <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detail Order</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
  </div>
  <div class="box-body">
  <!-- Filter bulan dan tahun di tracking order -->
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
            <div class="col-sm-4">
                <select name="month" id="month" class="form-control" required>
                    <option value="">Pilih bulan</option>
                    <?php for( $m=1; $m<=12; ++$m ) { 
                        $month_label = date('F', mktime(0, 0, 0, $m, 1));
                        ?>
                        <option value="<?php echo $m; ?>"><?php echo $month_label; ?></option>
                    <?php } ?>
                </select>
            </div>

            <div class="col-sm-4">
            <?php
                //get the current year
                $Startyear=date('Y');
                $endYear=$Startyear-10;

                // set start and end year range i.e the start year
                $yearArray = range($Startyear,$endYear);
                ?>
                <!-- here you displaying the dropdown list -->
                <select name="year" class="form-control" id='year' required>
                    <option value="">Pilih Tahun</option>
                    <?php
                    foreach ($yearArray as $year) {
                        // this allows you to select a particular year
                        echo '<option value="'.$year.'">'.$year.'</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4">
                <button class="btn btn-info" id='submitFilter'>Submit</button>
            </div>
        </div>
      </div>      
    </div>
    <label for="hint_filter">Pencarian dapat dipilih salah satu (bulan saja atau tahun saja) atau keduanya.</label>
    <br><br>
    <?php echo $this->session->flashdata('status');?>

    <table id="mytable" class="table table-bordered table-striped">
      <thead>
        <tr>      
          <th>No</th>
          <th>ID Order</th>
          <th>Nama Konsumen</th>
          <th>No Handphone</th>
          <th>Tanggal Order</th>
          <th>Tanggal Pengambilan</th>
          <th>Deadline</th>
          <th>Desainer</th>
          <th>Detail</th>
          <th>Aksi</th>
          <th>Hapus</th>
        </tr>
      </thead>
    </table>
  </div>
  <!-- /.box-body -->
</div>

<script src="<?= base_url('assets/order/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/plugins/fastclick/fastclick.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {"url": "<?php echo base_url();?>order/json", "type": "POST"},
            columns: [
                {
                    "data": "id_detail_order",
                    "orderable": false
                },
                {"data": "id_detail_order"},
                {"data": "nama_konsumen"},
                {"data": "hp_konsumen"},
                {"data": "tanggal_order"},
                {"data": "tanggal_pengambilan"},
                {"data": "selisih"},
                {"data": "nama"},
                {
                    "data": "action",
                    "orderable": false
                },
                {
                    "data": "yuk",
                    "orderable": false
                },
                {
                    "data": "id_order",
                    "orderable": false
                }

            ],
            order: [[5, 'desc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });

    $("#submitFilter").click(function() {
        var bulan = $("#month").val();
        var tahun = $("#year").val();

        // alert(bulan +"-"+ tahun);
        drawDataTableOrder(bulan, tahun);
    });

    function drawDataTableOrder(month, year){
        $('#mytable').dataTable().fnDestroy();

        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };
    
        var t = $("#mytable").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                        .off('.DT')
                        .on('keyup.DT', function(e) {
                            if (e.keyCode == 13) {
                                api.search(this.value).draw();
                    }
                });
            },
            oLanguage: {
                sProcessing: "loading..."
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "<?php echo base_url();?>order/jsonWithParams", 
                "type": "POST",
                "data": {
                            "year": year,
                            "month": month
                        }
                },
            columns: [
                {
                    "data": "id_detail_order",
                    "orderable": false
                },
                {"data": "id_detail_order"},
                {"data": "nama_konsumen"},
                {"data": "hp_konsumen"},
                {"data": "tanggal_order"},
                {"data": "tanggal_pengambilan"},
                {"data": "selisih"},
                {"data": "nama"},
                {
                    "data": "action",
                    "orderable": false
                },
                {
                    "data": "yuk",
                    "orderable": false
                },
                {
                    "data": "id_order",
                    "orderable": false
                }

            ],
            order: [[5, 'desc']],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    }
</script>
<script type="text/javascript">
  

  $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#myModal").modal('show');
                $.post('<?php echo base_url(); ?>order/tracking_detail_by_id',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
            });
        });

  function konfirmasi(){
    var pilihan=confirm("Apakah Anda yakin ingin menghapus?");
    if (pilihan) {
      return true;
    }
    else{
      return false;
    }
  }
</script>    

