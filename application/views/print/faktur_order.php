<!DOCTYPE html>
<html>
<head>
  <base target='_blank'/>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dezainla.com | Tanda Terima Order Untuk Konsumen</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="User Image" style="height:10%;width:10%"> DEZAINLA.COM
          <small><center>Alamat: Jl. R.A Abusamah No. 87 Kel. Sukabangun II, Kec. Sukarame, Palembang</center></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- Table row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          <?php
          foreach ($user->result_array() as $user_item) {
            # code...
          ?>
          <table style="font-size:11px">
            <tr>
              <td>ID Order</td>
              <td> :&ensp; </td>
              <td><?php echo $user_item['id_detail_order']; ?></td>
            </tr>
            <tr>
              <td>Tanggal Order</td>
              <td> : </td>
              <td><?php echo date('d-m-Y',strtotime($user_item['tanggal_order'])); ?></td>
            </tr>
            <tr>
              <td>Nama Konsumen</td>
              <td> : </td>
              <td><?php echo $user_item['nama_konsumen']; ?></td>
            </tr>
            <tr>
              <td>HP Konsumen</td>
              <td> : </td>
              <td><?php echo $user_item['hp_konsumen']; ?></td>
            </tr>
          </table>
        </div>

        <div class="col-sm-3 invoice-col">
        </div>
        <!-- /.col -->
        <div class="col-sm-5 invoice-col">
          <table style="font-size:11px">
            <tr>
              <td>Penerima File</td>
              <td> :&ensp; </td>
              <td><?php echo $user_item['nama']; ?></td>
            </tr>
            <tr>
              <td>Estimasi Waktu Selesai</td>
            </tr>
            <tr>
              <td>Tanggal</td>
              <td> : </td>
              <td><?php echo date('d-m-Y',strtotime($user_item['tanggal_pengambilan'])); ?></td>
            </tr>
            <tr>
              <td>Waktu</td>
              <td> : </td>
              <td><?php echo $user_item['waktu_pengambilan']; ?></td>
            </tr>
          </table>
          <?php
          }
          ?>
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped" style="font-size:11px">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Produk</th>
            <th>Jenis Produk</th>
            <th>Kuantitas</th>
            <th>Ukuran</th>
            <th>Harga</th>
            <th>Subtotal</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no=1;
          foreach ($detail_order->result_array() as $detail_order_item) {
            # code...
          ?>
          <tr>
            <td><?php echo $no++; ?></td>
            <td><?php echo $detail_order_item['id_detail_produk']; ?></td>
            <td><?php echo $detail_order_item['jenis_produk']; ?></td>
            <td><?php echo $detail_order_item['kuantitas']; ?></td>
            <td>
              <?php
              if ($detail_order_item['panjang']*$detail_order_item['lebar'] * $detail_order_item['kuantitas'] ==0) {
                # code...
                echo "";
              }
              else{
                echo $detail_order_item['panjang']." x ".$detail_order_item['lebar'];
              }
              ?>
            </td>
            <td><?php echo number_format($detail_order_item['harga_akhir'],"2",",",".") ; ?></td>
            <td><?php echo number_format($detail_order_item['subtotal'],"2",",","."); ?></td>
          </tr>
          <?php
          }
          ?>
        </tbody>
        </table>
      </div>
    </div>
    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-5">
          <table class="table table-striped" style="font-size:11px">
            <tr>
              <th>Setting</th>
              <th>Produksi</th>
              <th>Finishing</th>
            </tr>
            <tr>
              <td>..............</td>
              <td>..............</td>
              <td>..............</td>
            </tr>
            <tr>
              <td colspan="3">
                Keterangan
              </td>
            </tr>
            <tr>
              <td colspan="3">
              <?php echo $keterangan; ?>
              </td>
            </tr>
          </table>
        </div>
        <!-- /.col -->
        <div class="col-xs-7">
          <div class="table-responsive">
            <?php
            foreach ($total->result_array() as $total_item) {
              # code...
              ?>
              <table class="table" style="font-size:11px">
              <tr>
                <th style="width:50%">Total Bayar</th>
                <td>:</td>
                <td>Rp. <?php echo number_format($total_item['total_bayar'],"2",",",".") ;?></td>
              </tr>
              <tr>
                <th>Uang Muka</th>
                <td>:</td>
                <td>Rp. <?php echo number_format($total_item['uang_muka'],"2",",",".");?></td>
              </tr>
              <tr>
                <th>Diskon</th>
                <td>:</td>
                <td>Rp. <?php echo number_format($total_item['diskon'],"2",",",".");?></td>
              </tr>
              <tr>
                <th>Sisa Pembayaran</th>
                <td>:</td>
                <td><?php if($total_item['sisa_pembayaran']==0){echo "LUNAS";}else{echo"Rp. ".number_format($total_item['sisa_pembayaran'],"2",",",".");}?></td>
              </tr>
            </table>
            <?php
            }
            ?>
          </div>
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-5 col-xs-offset-1">
          <table style="font-size:11px">
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>Konsumen</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo $nama_konsumen; ?></td>
            </tr>
          </table>
        </div>
        <!-- /.col -->
        <div class="col-xs-5 col-xs-offset-1">
          <table style="font-size:11px">
            <tr>
              <td>Hormat Kami,</td>
            </tr>
            <tr>
              <td>Palembang, <?php echo date('d-m-Y'); ?></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td><?php echo $admin; ?></td>
            </tr>
          </table>
        </div>
        <!-- /.col -->
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
