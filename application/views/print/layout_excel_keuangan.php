<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=$title.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
 
 <table>
      <thead>
        <tr>      
          <th>No</th>
          <th>Tanggal Order</th>
          <th>ID Order</th>
          <th>Nama Konsumen</th>
          <th>No Handphone</th>
          <th>Penerima File</th>
          <th>Total Bayar</th>
          <th>Uang Muka</th>
          <th>Diskon</th>
          <th>Sisa Pembayaran</th>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<th>HPP</th>';
          } ?>
        </tr>
      </thead>
      <tbody>
        <?php
        $no=1;
        $total_harga=0;
        $sisa_pembayaran=0;
        $uang_muka=0;
        $diskon=0;
        $hpp = 0;
        foreach ($data->result_array() as $rekapitulasi_keuangan_item) {
          if($rekapitulasi_keuangan_item['sisa_pembayaran']<0){
            $rekapitulasi_keuangan_item['sisa_pembayaran'] *= -1;
          }
        ?>
        <tr>
          <td><?php echo $no++;?></td>
          <td><?php echo date('d-m-Y',strtotime($rekapitulasi_keuangan_item['tanggal_order'])); ?></td>    
          <td><?php echo $rekapitulasi_keuangan_item['id_detail_order']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['nama_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['hp_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['nama']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['total_bayar']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['uang_muka']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['diskon'];  ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['sisa_pembayaran']; ?></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td>'.$rekapitulasi_keuangan_item['hpp'].'</td>';
          } ?>        
        </tr>
        <?php
        $total_harga += $rekapitulasi_keuangan_item['total_bayar'];
        $sisa_pembayaran += $rekapitulasi_keuangan_item['sisa_pembayaran'];
        $uang_muka += $rekapitulasi_keuangan_item['uang_muka'];
        $diskon += $rekapitulasi_keuangan_item['diskon'];
        $hpp += $rekapitulasi_keuangan_item['hpp'];
        }
        ?>
        <tr>
          <td colspan="6" align="center"><b>TOTAL</b></td>
          <td><b>Rp. <?php echo number_format($total_harga, "2", ",", "."); ?></b></td>
          <td><b>Rp. <?php echo number_format($uang_muka, "2", ",", "."); ?></b></td>
          <td><b>Rp. <?php echo number_format($diskon, "2", ",", "."); ?></b></td>
          <td><b>Rp. <?php echo number_format($sisa_pembayaran, "2", ",", "."); ?></b></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td><b>Rp. '.number_format($hpp, "2", ",", ".").'</b></td>';
          } ?>   
        </tr>
     </tbody>
    </table>