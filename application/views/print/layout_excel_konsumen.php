<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=$title.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
 
 <table>
      <thead>
        <tr>      
          <th>No</th>
          <th>Nama Konsumen</th>
          <th>No Handphone</th>
          <th>Total Order</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no=1;
        foreach ($data->result_array() as $row) {
        ?>
        <tr>
          <td><?php echo $no++;?></td>
          <td><?php echo $row['nama_konsumen']; ?></td>
          <td><?php echo $row['hp_konsumen']; ?></td>
          <td><?php echo $row['total_order']; ?></td>
        </tr>
        <?php 
        }
        ?>
     </tbody>
    </table>