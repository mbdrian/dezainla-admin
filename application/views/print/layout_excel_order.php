<?php
 
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=$title.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");
 
 ?>
 
 <table width="100%">
      <thead>
           <tr>
                <th width='100%'>Tanggal Order</th>
                <th width='100%'>ID Order</th>
                <th width='100%'>Jenis Produk</th>
                <th width='100%'>Nama Konsumen</th>
                <th width='100%'>No Handphone</th>
                <th width='100%'>Panjang</th>
                <th width='100%'>Lebar</th>
                <th width='100%'>Luas</th>
                <th width='100%'>Quantity</th>
                <th width='100%'>Total item</th>
                <th width='100%'>Sub Total</th>
                <th width='100%'>Diskon</th>
                <th width='100%'>Sisa Pembayaran</th>
                <th width='100%'>Penerima File</th>
                <th width='100%'>Nama Produk</th>
                <th width='100%'>Keterangan</th>
                <?php if($this->session->userdata('level')=='ceo'){ ?>
                    <th width='100%'>HPP</th>
                <?php } ?>
           </tr>
 
      </thead>
 
      <tbody>
        <?php
        $no=1;
        $total_harga=0;
        // $uang_muka=0;s
        $sisa_pembayaran=0;
        $diskon = 0;
        $hpp = 0;
        foreach ($data->result_array() as $rekapitulasi_order_item) {
          $luas = $rekapitulasi_order_item['panjang']*$rekapitulasi_order_item['lebar'];
          $total_item = $luas * $rekapitulasi_order_item['kuantitas'];

          if($rekapitulasi_order_item['sisa_pembayaran']<0){
            $rekapitulasi_order_item['sisa_pembayaran'] *= -1;
          }
        ?>
        <tr>    
          <td><?php echo date('d-m-Y',strtotime($rekapitulasi_order_item['tanggal_order'])); ?></td>
          <td><?php echo $rekapitulasi_order_item['id_detail_order']; ?></td>
          <td><?php echo $rekapitulasi_order_item['jenis_produk']; ?></td>
          <td><?php echo $rekapitulasi_order_item['nama_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_order_item['hp_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_order_item['panjang']; ?></td>
          <td><?php echo $rekapitulasi_order_item['lebar']; ?></td>
          <td><?php echo $luas; ?></td>
          <td><?php echo $rekapitulasi_order_item['kuantitas']; ?></td>
          <td><?php echo $total_item; ?></td>
          <td><?php echo $rekapitulasi_order_item['subtotal']; ?></td>
          <td><?php echo $rekapitulasi_order_item['diskon'];  ?></td>
          <td><?php echo $rekapitulasi_order_item['sisa_pembayaran']; ?></td>
          <td><?php echo $rekapitulasi_order_item['nama']; ?></td> 
          <td><?php echo $rekapitulasi_order_item['nama_produk']; ?></td>
          <td><?php echo $rekapitulasi_order_item['keterangan']; ?></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td>'.$rekapitulasi_order_item['hpp'].'</td>';
          } ?>
        </tr>
        <?php
        @$total_harga += $rekapitulasi_order_item['subtotal'];
        // @$uang_muka += $rekapitulasi_order_item['uang_muka'];
        @$sisa_pembayaran += $rekapitulasi_order_item['sisa_pembayaran'];
        @$diskon += $rekapitulasi_order_item['diskon'];
        @$hpp += $rekapitulasi_order_item['hpp'];
        }
        ?>
        <tr>
          <td colspan="10"><b>TOTAL</b></td>
          <td><b><?php echo @$total_harga; ?></b></td>
          <td><b><?php echo @$diskon; ?></b></td>
          <td><b><?php echo @$sisa_pembayaran; ?></b></td>
          <td colspan="2"></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td><b>'.@$hpp.'</b></td>';
          } ?>
        </tr>
     </tbody>
 
 </table>