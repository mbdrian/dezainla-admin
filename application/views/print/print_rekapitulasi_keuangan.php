<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dezainla.com | Rekapitulasi Order</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="window.open();window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <img src="<?php echo base_url(); ?>assets/images/logo.png" alt="User Image" style="height:20px;width:20px"> Dezainla.com
          <small class="pull-right"><?php echo date('d/m/Y');?></small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- Table row -->
    <div class="row">
      <div class="col-md-12 table-responsive">
       <table class="table table-striped">
       <thead>
        <tr>      
          <th>No</th>
          <th>Tanggal Order</th>
          <th>ID Order</th>
          <th>Nama Konsumen</th>
          <th>No Handphone</th>
          <th>Penerima File</th>
          <th>Total Bayar</th>
          <th>Uang Muka</th>
          <th>Diskon</th>
          <th>Sisa Pembayaran</th>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<th>HPP</th>';
          } ?>
        </tr>
      </thead>
      <tbody>
        <?php
        $no=1;
        $total_harga=0;
        $sisa_pembayaran=0;
        $uang_muka=0;
        $diskon=0;
        $hpp = 0;
        foreach ($rekapitulasi_keuangan->result_array() as $rekapitulasi_keuangan_item) {
          if($rekapitulasi_keuangan_item['sisa_pembayaran']<0){
            $rekapitulasi_keuangan_item['sisa_pembayaran'] *= -1;
          }
        ?>
        <tr>
          <td><?php echo $no++;?></td>
          <td><?php echo date('d-m-Y',strtotime($rekapitulasi_keuangan_item['tanggal_order'])); ?></td>    
          <td><?php echo $rekapitulasi_keuangan_item['id_detail_order']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['nama_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['hp_konsumen']; ?></td>
          <td><?php echo $rekapitulasi_keuangan_item['nama']; ?></td>
          <td>Rp. <?php echo number_format($rekapitulasi_keuangan_item['total_bayar'], "2", ",", "."); ?></td>
          <td>Rp. <?php echo number_format($rekapitulasi_keuangan_item['uang_muka'], "2", ",", "."); ?></td>
          <td><?php echo "Rp. ".number_format($rekapitulasi_keuangan_item['diskon'], "2", ",", ".");  ?></td>
          <td><?php echo "Rp. ". number_format($rekapitulasi_keuangan_item['sisa_pembayaran'], "2", ",", "."); ?></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td>Rp. '.number_format($rekapitulasi_keuangan_item['hpp'], "2", ",", ".").'</td>';
          } ?>        
        </tr>
        <?php
        $total_harga += $rekapitulasi_keuangan_item['total_bayar'];
        $sisa_pembayaran += $rekapitulasi_keuangan_item['sisa_pembayaran'];
        $uang_muka += $rekapitulasi_keuangan_item['uang_muka'];
        $diskon += $rekapitulasi_keuangan_item['diskon'];
        $hpp += $rekapitulasi_keuangan_item['hpp'];
        }
        ?>
        <tr>
          <td colspan="6" align="center"><b>TOTAL</b></td>
          <td><b>Rp. <?php echo number_format($total_harga, "2", ",", "."); ?></b></td>
          <td><b>Rp. <?php echo number_format($uang_muka, "2", ",", "."); ?></b></td>
          <td><b>Rp. <?php echo number_format($diskon, "2", ",", "."); ?></b></td>
          <td><b>Rp. <?php echo number_format($sisa_pembayaran, "2", ",", "."); ?></b></td>
          <?php if($this->session->userdata('level')=='ceo'){
            echo '<td><b>Rp. '.number_format($hpp, "2", ",", ".").'</b></td>';
          } ?>   
        </tr>
     </tbody>
    </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <!-- /.col -->
      <div class="col-xs-6 col-xs-offset-6">
        <p class="lead">Data Penjualan pada tanggal <?php echo date('d/m/Y',strtotime($tanggal_mulai)); ?> sampai tanggal <?php echo date('d/m/Y',strtotime($tanggal_selesai)); ?></p>

        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Total Pendapatan</th>
              <td><?php echo 'Rp. '.number_format($total_harga, "2", ",", "."); ?></td>
            </tr>
            <tr>
              <th style="width:50%">Total Uang Muka</th>
              <td><?php echo 'Rp. '.number_format($uang_muka, "2", ",", "."); ?></td>
            </tr>
            <tr>
              <th>Total Sisa Pembayaran</th>
              <td><?php echo 'Rp. '.number_format($sisa_pembayaran, "2", ",", "."); ?></td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
