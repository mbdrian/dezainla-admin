<?php
		$tanggal=$this->input->post('tanggal');
		$tanggal_pengambilan=date('d-m-Y',strtotime($this->input->post('tanggal_pengambilan')));
		$waktu_pengambilan=$this->input->post('waktu_pengambilan');
		$tanggal_baru=tgl_indo(date('Y-m-d',strtotime($tanggal)));
		$nama_konsumen=$this->input->post('nama_konsumen');
		$hp_konsumen=$this->input->post('hp_konsumen');
		$total=$this->input->post('total');
		$bayar=$this->input->post('bayar');
		$kembali=$this->input->post('kembali');
		$nama_penerima_file=$this->input->post('nama_penerima_file');
		$string=strtoupper(substr(md5(rand()), 0, 7)) ;

		$image=base_url().'/assets/images/logo.jpg';
		$this->fpdf->FPDF('L','cm','A5');
		$this->fpdf->AddPage();
		$this->fpdf->SetMargins(2,2,2);
		$this->fpdf->Ln();
		$this->fpdf->setFont('Arial','B',9);
		$this->fpdf->Text(9,1,'FAKTUR ORDER','C');//left//top
		$this->fpdf->setFont('Arial','B',9);
		$this->fpdf->Text(7.5,1.5,'Tanda Terima Order Untuk Konsumen','C');//left//top
		$this->fpdf->setFont('Arial','',7);
		$this->fpdf->Text(6,2,'Alamat: Jl. R.A Abusamah No. 87 Kel. Sukabangun II, Kec. Sukarame, Palembang','C');//left//top
		$this->fpdf->setFont('Arial','',7);
		$this->fpdf->Text(8.1,1.9,'');
		$this->fpdf->Line(20.3,2.1,0.5,2.1); 
		$this->fpdf->Cell(0,0,$this->fpdf->Image($image,1, 0.1, 2), 0, 0, 'L', false );
		$this->fpdf->ln(2.0);
		$this->fpdf->Text(2,3,'ID Order','C');//left//top
		$this->fpdf->Text(2,3.5,'Tanggal','C');//left//top
		$this->fpdf->Text(2,4,'Konsumen','C');//left//top
		$this->fpdf->Text(2,4.5,'Telepon','C');//left//top
		$this->fpdf->Text(3.5,3,': '.$string.'','C');//left//top
		$this->fpdf->Text(3.5,3.5,': '.date('d-m-Y',strtotime($tanggal)).'','C');//left//top
		$this->fpdf->Text(3.5,4,': '.$nama_konsumen.'','C');//left//top
		$this->fpdf->Text(3.5,4.5,': '.$hp_konsumen.'','C');//left//top
		$this->fpdf->Text(15,3,'Penerima File ','C');//left//top
		$this->fpdf->Text(15,3.5,'Estimasi Selesai Pengerjaan','C');//left//top
		$this->fpdf->Text(15,4,'Tanggal','C');//left//top
		$this->fpdf->Text(15,4.5,'Waktu','C');//left//top
		$this->fpdf->Text(16.5,3,' : '.$nama_penerima_file.'','C');//left//top
		$this->fpdf->Text(16.5,4,': '.$tanggal_pengambilan.'','C');//left//top
		$this->fpdf->Text(16.5,4.5,': '.$waktu_pengambilan.'','C');//left//top
		$this->fpdf->ln(2.0);
		$this->fpdf->setFont('Arial','B',7);
		$this->fpdf->Cell(1,0.5,'No',1,0,'C');
		$this->fpdf->Cell(3,0.5,'Jenis Order',1,0,'C');
		$this->fpdf->Cell(3,0.5,'Ukuran',1,0,'C');
		$this->fpdf->Cell(3,0.5,'Kuantitas',1,0,'C');
		$this->fpdf->Cell(3,0.5,'Harga',1,0,'C');
		$this->fpdf->Cell(3,0.5,'Subtotal',1,0,'C');
		$this->fpdf->Ln();
		$this->fpdf->setFont('Arial','',7);
		$no=1;
		foreach ($this->cart->contents() as $items) {
			# code...
			$nama_barang=$items["name"];
			$panjang=$items["panjang"];
			$lebar=$items["lebar"];
			$kuantitas=$items["qty"];
			$harga=$items["price"];
			$subtotal=$items["panjang"]*$items["lebar"]*$items["qty"]*$items["price"];
			$this->fpdf->Cell(1,0.5,''.$no++.'',1,0,'C');
			$this->fpdf->Cell(3,0.5,''.$nama_barang.'',1,0,'C');
			$this->fpdf->Cell(3,0.5,''.$panjang.' x '.$lebar.'',1,0,'C');
			$this->fpdf->Cell(3,0.5,''.$kuantitas.'',1,0,'C');
			$this->fpdf->Cell(3,0.5,''.$harga.'',1,0,'C');
			$this->fpdf->Cell(3,0.5,''.$subtotal.'',1,0,'C');
			$this->fpdf->Ln();
		}
		
		$this->fpdf->Text(15,10,'Total Jumlah','C');//left//top
		$this->fpdf->Text(15,10.5,'Uang Muka','C');//left//top
		$this->fpdf->Text(15,11,'Sisa Pembayaran','C');//left//top
		$this->fpdf->Text(17,10,' : '.$total.'','C');//left//top
		$this->fpdf->Text(17,10.5,' : '.$bayar.'','C');//left//top
		$this->fpdf->Text(17,11,' : '.$kembali.'','C');//left//top
		$this->fpdf->Ln(1.0);
		$this->fpdf->setFont('Arial','',7);
		$this->fpdf->Text(15,12,'Hormat Kami,','C');//left//top
		$this->fpdf->Text(14.5,12.5,'Palembang, '.$tanggal_baru.'','C');//left//top
		$this->fpdf->Ln(1.0);
		$this->fpdf->Text(2,14,''.$nama_konsumen.'','C');//left//top
		$this->fpdf->Text(15,14,''.$nama_penerima_file.'','C');//left//top
		$this->fpdf->Output("".$string."".$nama_konsumen.".pdf","I");
		$this->cart->destroy();
?>