<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dezainla | Administrator</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
  <!-- jQuery 2.2.3 -->
  <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url(); ?>assets/images/logo.png" style="width:60px;height=60px"><b>dezainla</b>.com</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo base_url(); ?>assets/images/logo.png" style="width:60px;height=60px"><b>dezainla.com</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
         
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php echo $username; ?></span>
            </a>
          </li>
           <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><?php echo date('d/m/Y') ?></span>
            </a>
          </li>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs"><div id="clock"></div></span>
            </a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header"><center>MAIN NAVIGATION</center></li>
        <?php
        if ($level=='admin') {
          # code...
        ?>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <!--
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Profil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>order/rekapitulasi_order_by_user"><i class="fa fa-circle-o"></i> Rekapitulasi Order Saya</a></li>
            </ul>
        </li>
        -->
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order">
            <i class="fa fa-shopping-cart"></i> <span>Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order/rekapitulasi_order">
            <i class="fa fa-reorder"></i> <span>Rekapitulasi Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order/tracking_order">
            <i class="fa fa-opencart"></i> <span>Cek Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-send"></i> <span>Tracking Deadline</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>order/tracking_deadline"><i class="fa fa-circle-o"></i> Tracking Deadline Terdekat</a></li>
            <li><a href="<?php echo base_url(); ?>order/tracking_deadline_lewat"><i class="fa fa-circle-o"></i> Tracking Deadline Lewat</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order/tracking_order_status">
            <i class="fa fa-opencart"></i> <span>Tracking Order Status</span>
          </a>
        </li>
         <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Kategori Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>produk/daftar_master_produk"><i class="fa fa-circle-o"></i> Daftar Kategori Produk</a></li>
            <li><a href="<?php echo base_url(); ?>produk/tambah_master_produk"><i class="fa fa-circle-o"></i> Tambah Kategori produk</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i> <span>Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>produk/daftar_produk"><i class="fa fa-circle-o"></i> Daftar Produk</a></li>
            <li><a href="<?php echo base_url(); ?>produk/tambah_produk"><i class="fa fa-circle-o"></i> Tambah produk</a></li>
          </ul>
        </li> -->
        <li class="treeview">
          <a href="<?php echo base_url(); ?>auth/logout">
            <i class="fa  fa-sign-out"></i> <span>Sign Out</span>
          </a>
        </li>
        <?php
        }
        elseif($level=='produksi'){
        ?>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order/tracking_order_status">
            <i class="fa fa-opencart"></i> <span>Tracking Order Status</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-send"></i> <span>Tracking Deadline</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>order/tracking_deadline"><i class="fa fa-circle-o"></i> Tracking Deadline Terdekat</a></li>
            <li><a href="<?php echo base_url(); ?>order/tracking_deadline_lewat"><i class="fa fa-circle-o"></i> Tracking Deadline Lewat</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Kategori Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>produk/daftar_master_produk"><i class="fa fa-circle-o"></i> Daftar Kateogir Produk</a></li>
            <li><a href="<?php echo base_url(); ?>produk/tambah_master_produk"><i class="fa fa-circle-o"></i> Tambah Kategori produk</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i> <span>Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>produk/daftar_produk"><i class="fa fa-circle-o"></i> Daftar Produk</a></li>
            <li><a href="<?php echo base_url(); ?>produk/tambah_produk"><i class="fa fa-circle-o"></i> Tambah produk</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>auth/logout">
            <i class="fa  fa-sign-out"></i> <span>Sign Out</span>
          </a>
        </li>
        <?php
        }
        elseif ($level=='desainer') {
          # code...
        ?>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <!--
         <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Profil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>order/rekapitulasi_order_by_user"><i class="fa fa-circle-o"></i> Rekapitulasi Order Saya</a></li>
            <li><a href="<?php echo base_url(); ?>user/ganti_password"><i class="fa fa-circle-o"></i> Ganti Password</a></li>
          </ul>
        </li>
        -->
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order">
            <i class="fa fa-shopping-cart"></i> <span>Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order/tracking_order">
            <i class="fa fa-opencart"></i> <span>Tracking Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>auth/logout">
            <i class="fa  fa-sign-out"></i> <span>Sign Out</span>
          </a>
        </li>
        <?php
        }
        elseif ($level=='ceo') {
          # code...
        ?>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order">
            <i class="fa fa-shopping-cart"></i> <span>Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-reorder"></i> <span>Rekapitulasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>order/rekapitulasi_order"><i class='fa fa-circle-o'></i> Rekapitulasi Order</a></li>
            <li><a href="<?php echo base_url(); ?>order/rekapitulasi_keuangan"><i class="fa fa-circle-o"></i>Rekapitulasi Keuangan</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order/tracking_order">
            <i class="fa fa-opencart"></i> <span>Cek Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-send"></i> <span>Tracking Deadline</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>order/tracking_deadline"><i class="fa fa-circle-o"></i> Tracking Deadline Terdekat</a></li>
            <li><a href="<?php echo base_url(); ?>order/tracking_deadline_lewat"><i class="fa fa-circle-o"></i> Tracking Deadline Lewat</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>order/tracking_order_status">
            <i class="fa fa-shopping-cart"></i> <span>Tracking Status Order</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Kategori Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>produk/daftar_master_produk"><i class="fa fa-circle-o"></i> Daftar Kategori Produk</a></li>
            <li><a href="<?php echo base_url(); ?>produk/tambah_master_produk"><i class="fa fa-circle-o"></i> Tambah Kategori produk</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-th"></i> <span>Produk</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>produk/daftar_produk"><i class="fa fa-circle-o"></i> Daftar Produk</a></li>
            <li><a href="<?php echo base_url(); ?>produk/tambah_produk"><i class="fa fa-circle-o"></i> Tambah produk</a></li>
          </ul>
        </li>
            <li class="header">MASTER</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-circle-o"></i> <span>Kepegawaian</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo base_url(); ?>user"><i class="fa fa-circle-o"></i> Data Pegawai</a></li>
                    <li><a href="<?php echo base_url(); ?>user/rekapitulasi_order_user"><i class="fa fa-circle-o"></i> Rekapitulasi Order User</a></li>
                </ul>
            </li>
        <li class="treeview">
          <a href="<?php echo base_url(); ?>auth/logout">
            <i class="fa  fa-sign-out"></i> <span>Sign Out</span>
          </a>
        </li>
        <?php
        }
        ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
<script type="text/javascript">

function startTime() {
    var today=new Date(),
        curr_hour=today.getHours(),
        curr_min=today.getMinutes(),
        curr_sec=today.getSeconds();
    curr_hour=checkTime(curr_hour);
    curr_min=checkTime(curr_min);
    curr_sec=checkTime(curr_sec);
    document.getElementById('clock').innerHTML=curr_hour+":"+curr_min+":"+curr_sec;
}
function checkTime(i) {
    if (i<10) {
        i="0" + i;
    }
    return i;
}
setInterval(startTime, 500);
</script>