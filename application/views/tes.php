<!doctype html>
<html>
    <head>
        <title>Modal - harviacode.com</title>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.css"/>
    </head>
    <body>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="simpan btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <table class="table">
                <tr>
                    <th>Jenis Produk</th>
                    <th>Harga</th>
                    <th>Keterangan</th>
                    <th>Aksi</th>
                </tr>
                <!--pada prakteknya looping dari database-->
                <?php
                foreach ($detail_produk->result() as $row) {
                  # code...
                ?>
                <tr>
                  <td><?php echo $row->jenis_produk; ?></td>
                  <td><?php echo $row->harga_produk; ?></td>
                  <td><?php echo $row->keterangan; ?></td>
                  <td><a href="#" class="edit-record" data-id="<?php echo $row->id_detail_produk; ?>">Show</a></td>
                </tr>
                <?php
                }
                ?>
            </table>
        </div>
        <script src="<?= base_url('assets/order/jquery-2.1.4.min.js') ?>"></script>
        <script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.js"></script>
        <script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#myModal").modal('show');
                $.post('<?php echo base_url(); ?>order/proses_tes',
                    {id:$(this).attr('data-id')},
                    function(html){
                        $(".modal-body").html(html);
                    }   
                );
            });
        });
    </script>
    </body>
</html>